> Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

> Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

> 1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

> 2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

> 3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------


Most crypto-ICs are vulnerable to a potent class of physical
attacks known as fault injection attacks. In these attacks, sensitive information
like a cipher's secret key can be easily gleaned by faults injected during
the device's operations. These attacks will only be successful if the faults are
injected at precise locations and have well defined properties. Identifying the
fault sensitive regions of a hardware design is difficult and typically done
through manual inspection. 

This repo provides the first automated tools to evaluate cypto-ciphers for
their fault attack resistance.  This repository contains  the code for three 
frameworks, namely XFC, SAFARI and FEDS.

**XFC.**
Exploitable Fault Characterization (XFC) is a framework to identify locations
in a block cipher that are vulnerable to fault injection attacks. 

*Input:*   Block Cipher Specification and a location in the specification

*Output:*  Vulnerability of that location, i.e., number of key bytes which 
           can be retrieved and the corresponding complexity.

*Publication:* 
[XFC : A Framework for Exploitable Fault Characterization in Block Ciphers](https://dl.acm.org/citation.cfm?id=3062340), DAC 2017.



**SAFARI.**
SAFARI builds on XFC. It can automatically synthesize fault attack resistant
C and Verilog implementations of block ciphers. Based on a user specified
security parameter, appropriate countermeasures are added to  locations in 
the block cipher. Details about input and output are given in the folder. 

*Input:*  Block Cipher Specification and a user-specified security parameter

*Output:*  Fault attack resistant C and Verilog code

*Publication:*  [SAFARI: Automatic Synthesis of Fault-Attack Resistant Block Cipher Implementations](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8634921),
IEEE TCAD, 2019.

**FEDS.**
FEDS is an automated fault attack exploitability detection tool that works on software implementation of block ciphers. FEDS can automatically map vulnerable locations (output of XFC) tools like XFC to software implementation of the block ciphers and then finds all the possible fault exploitable instructions at the IR level using a LLVM parser. FEDS can also provide appropriate countermeasures at the IR level and generate fault attack resistant executables. Details about input and output are given in the folder. 

*Input:*  Block Cipher Specification and software Implementation of Block Cipher

*Output:*  All fault exploitable instructions that can retrieve the key with complexity provided by HLE Tools like XFC

*Publication:*  [FEDS: Comprehensive Fault Attack Exploitability Detection for Software Implementations of Block Ciphers],
TCHES, 2020.


Please feel free to post your comments to 
scsl [at] cse [dot] iitm [dot] ac [dot] in
