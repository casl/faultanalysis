"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This code synthesise BCS to Verilog
--------------------------------------------------------------------------------------------------
"""




import store_operands as so
import re

part_size = 8 #usually byte
parity_bits = 2 ## 1 parity bit for how many bits
symbol_dict = { "RS" : ">>" , "LS" : "<<" , "XOR" : "^" , "MUL" : "*" , "AND" : "&"  }
bits_dict = dict() #{ 1 : "127:120" , 2 : "119:112" , 3: "111:104" , 4:"103:96" , 5: "95:88" , 6: "87:80" , 7:"79:72", 8:"71:64" , 9:"63:56" , 10: "55:48" , 11:"47:40" , 12:"39:32" , 13: "31:24" , 14: "23:16" , 15: "15:8" , 16:"7:0" }
# bits_dict_64 = {  1:"63:56" , 2: "55:48" , 3:"47:40" , 4:"39:32" , 5: "31:24" , 6: "23:16" , 7: "15:8" , 8:"7:0" }


parity_funcs = set()
last_pop =''



def formBitdict(totalbytes):
    global bits_dict
    nm = totalbytes * part_size
    # print >> f2 , nm
    for x in xrange(0, totalbytes):
        val = str(nm-1) + ":" + str(nm-part_size)
        # print >> f2 , val
        nm= nm - part_size
        bits_dict[x+1]= val





def getbitvalues(num,c_t):
    # print >> f2 , "num " , num, c_t
    n = map(int, re.findall(r'\d+', str(num)))
    if str(num).startswith("F"):
        if n[0]==0:
            totalbytes = 16# "16"
        else:
            totalbytes = len(so.dep_var_lst[n[0]])
        nm = totalbytes * part_size
        nm = nm - (part_size*(n[1]-1))
        val = str(nm-1) + ":" + str(nm-part_size)
        # print >> f2 , val
        return val
    elif str(num).isdigit() and c_t != -1:
        # print >> f2 , "Here"
        # print >> f2 , num, "C = ", c_t
        totalbytes = c_t
        nm = totalbytes * part_size
        nm = nm - (part_size*(n[0]-1))
        # print >> f2 , "nm ",nm
        val = str(nm-1) + ":" + str(nm-part_size)
        # print >> f2 , val
        return val


######################################## This function form s statements of verilog code. either assignment or instantiation###########################################


# var can take following values:
# 1. XOR , ADD, MUL ,RS, LS : binary
# 2. LKUP :binary but needs special care
# 3. Other defined functions e.g. MUL2, MUL3, Parity , Compare etc.. : varies
#
# var_list is list of veriables used to form expressions
#
# func can take following values:
# 1. KEYXOR
# 2. SUBBYTE
# 3. MDS
# 4. SWAP
# 5. FINDPARITY
# 6. CMP
# 7. PREDICTPARITY


########################################################################################################################################
def opHandler (var , var_list,func , ys):
    # f2 = open("./out/output.v" , 'a')
    # print >> f2 , func
    # print >> f2 , var_list
    if str(var) in symbol_dict.keys():


        a = var_list.pop()
        b = var_list.pop()
        # print >> f2 , "a" , a
        # print >> f2 , "b" , b
        # print >> f2 , "var ", var
        n = map(int, re.findall(r'\d+', a))
        # print >> f2 , "n " ,n
        if (len(n) >= 4) and a.startswith("F") and b.startswith("F") and var !='XOR':
            instance_name = "PS" + str(ys)
            c_instance =  "ParitySbox " + instance_name + " ( " + a + " , X[ " + str(ys-1) + " ] ) ;"
            print >> f2 , c_instance
            newstr = b + symbol_dict[str(var)] + "X[ " + str(ys-1) + " ]"
        else:
            newstr = a + symbol_dict[str(var)] + b
        # print >> f2 , newstr
        var_list.append(newstr)
    elif str(var) in ('LKUP')  :
        # print >> f2 , func
    	a = var_list.pop()
        b = var_list.pop()
        # print >> f2 , "a ", a
        # print >> f2 , "b ", b
        if func == "KEYXOR":
            newstr = a + "[" + b + "]"
            var_list.append(newstr)
        elif func in ("SUBBYTE"):
            newstr = str(b)
            var_list.append(newstr)
        elif func in ("PREDICTPARITY"):
            # print >> f2 , "a : ",a
            # print >> f2 , "b : ", b
            n = map(int, re.findall(r'\d+', b))
            ln = len(n)
            if  a.startswith('K'):
                # print >> f2 , "step1" , a, b
                if  (not b.startswith("F")):
                    newstr =  a + "[ " + getbitvalues(b,len(so.lookup_variables[a])) + " ] "
                    # print >> f2 , newstr
                # else : #write later
            if ln==2 and a.startswith('PS'):
                # print >> f2 , "write now"
                if b.startswith("F") :
                    div = n[1]/ (part_size/parity_bits)
                    rem = n[1] % (part_size/parity_bits)
                    # print >> f2 , div , rem
                    newstr = "F" + str(n[0]) + "[" + getbitvalues(b,-1) + "] , " + bin(rem)
                    # print >> f2 , newstr
                # else: #write later

            var_list.append(newstr)
        else:
            newstr =  str(a) +"[" + str(b) + "]"
            var_list.append(newstr)

    else:
        # print >> f2 , so.new_function[var]
        ln = len(so.new_function[var])
        newstr = var + "( "
        tlist =[]
        for i in xrange (0, ln):

            a = var_list.pop()
            tlist.append(a)
        for i in xrange(len(tlist),0,-1):
            # a=tlist.pop()
            if i == 1:
                newstr = newstr + tlist[i-1]
            else:
                newstr = newstr + tlist[i-1] + ","
        newstr = newstr + " )"
        var_list.append(newstr)
########################################################################################################################################


###############################################3 Modifies indexes of the bytes/parts in the round function######################

# Here possible values in "num" are:
# 1.1. Lookup table name: KEY, SBOX etc (len(n) == 0)
# 1.2. part name : F1[1] , F1[2].... (len(n) == 2)
# 1.3. Interger : for accessing lookups or shift or anything (len(n) == 1 )
# 1.4. Parts of parts; bits from bytes. :: F1[0]_[0:1] , F2[3]_[6:7] etc (len(n) == 4)
# 1.5. Index with a function part :: F1[0]_[0], F4[8]_[4] etc  len(n) == 3
#
#
# In each of the above cases function , "fn" can be:
# 2.1. Normal round function
# 2.2. Find parity function
# 2.3. Parity Prediction function
# 2.4. Compare function
#
# 2.4 , 1.2 , 1.5 , 1,4 :  represent in 1 bit form
# 2.2 , 1.2, 1.4 : represent in bit range form
# 2.1 , 1.1 , 1.2 , 1.3, 1.4 : represent in bit range form
################################################################################################
def processForOriginal(num , fn, indexval):
    # f2 = open("./out/output.v" , 'a')
    # print >> f2 , parity_funcs
    # print >> f2 , "Start"
    # print >> f2 , num , fn
    n = map(int, re.findall(r'\d+', num))
    # print >> f2 , "n" , n
    ln = len(n)
    if fn not in ("FINDPARITY", "PREDICTPARITY" ,"CMP" ): # **************** 2.1
        # print >> f2 , "Type: Original round"
        if ln ==1 and not num.startswith("K") :
            c_var = getbitvalues(num , len(so.dep_var_lst[indexval]))
        elif ln == 2 and num.startswith("F"):
            c_var = "F"+str(int(n[0])) + "[" + getbitvalues(num,-1) + "]"
        # elif ln == 4: #yet to write
        #     pass
        else:
            # print >> f2 , "Here"
            c_var = num
    elif fn in ("FINDPARITY"): # **************** 2.2
        # print >> f2 , "Type: Find Parity"
        if ln == 2:
            c_var = c_var = "F"+str(int(n[0])) + "[" + getbitvalues(num,-1) + "]"
        elif ln == 4:
            # print >> f2 , "Write the code now"
            replace_var = bits_dict[n[1]]
            # print >> f2 , replace_var
            m = map(int, re.findall(r'\d+', replace_var))
            # print >> f2 , str(m[0]-n[2]) , str(m[0]-n[3])
            c_var = c_var = "F"+str(int(n[0])) + "[" + str(m[0]-n[2]) + ":" + str(m[0]-n[3]) + "]"
    elif fn in ("CMP"):  # **************** 2.4
        # print >> f2 , "Type: Compare"
        if ln == 2:
            nb = (part_size/parity_bits)
            if nb >1:
                c_var = "F" + str(int(n[0])) + "[" + str(((n[1]-1)*nb)+nb-1) + " : " + str((n[1]-1)*nb) + "]"
            else:

                c_var = "F" + str(int(n[0])) + "[" + str(n[1]-1) + "]"
            # c_var = "F"+str(int(n[0])) + "[" + str(n[1]-1) + "]"
        elif ln == 3:
            # print >> f2 , "Write the code now"
            index = ( (n[1]-1) * (part_size/parity_bits) ) + n[2]
            # print >> f2 , index
            c_var = "F"+str(int(n[0])) + "[" + str(index) + "]"
        # elif ln == 4: #yet to wite
        #     pass
        else:
            c_var = num
    elif fn in ("PREDICTPARITY"): # **************** 2.3
        # print >> f2 , "parity funcs ::: " ,parity_funcs
        # print >> f2 , "Type: Predict Parity" , num
        if ln > 1 and str(n[0]) not in parity_funcs  and num.startswith("F"):
            # print >> f2 , "Not in predict parity and starts with F"
            if ln == 2:
                c_var = c_var = "F"+str(int(n[0])) + "[" + getbitvalues(num,-1) + "]"
            elif ln ==3:
                index = ( (n[1]-1) * (part_size/parity_bits) ) + n[2]
                # print >> f2 , index
                c_var = "F"+str(int(n[0])) + "[" + str(index) + "]"
            elif ln == 4:
                replace_var = bits_dict[n[1]]
                # print >> f2 , replace_var
                m = map(int, re.findall(r'\d+', replace_var))
                # print >> f2 , str(m[0]-n[2]) , str(m[0]-n[3])
                c_var = c_var = "F"+str(int(n[0])) + "[" + str(m[0]-n[2]) + ":" + str(m[0]-n[3]) + "]"
        elif ln > 1 and str(n[0]) not in parity_funcs  and (not num.startswith("F")):
            # print >> f2 , "Not in predict parity and not starts with F"
            if ln == 2:
                index = ( (n[0]-1) * (part_size/parity_bits) ) + n[1]
                # print >> f2 , index
                c_var =   str(index)
            elif ln ==3: #write later
                # print >> f2 , "Write the code now"
                replace_var = bits_dict[n[0]]
                # print >> f2 , replace_var
                m = map(int, re.findall(r'\d+', replace_var))
                # print >> f2 , str(m[0]-n[1]) , str(m[0]-n[2])
                c_var = c_var =   str(m[0]-n[1]) + ":" + str(m[0]-n[2])
            # elif ln == 4: #write later
            #     pass
        elif ln > 1 and str(n[0])  in parity_funcs and num.startswith("F"):
            # print >> f2 , " in predict parity "
            if ln == 2:
                nb = (part_size/parity_bits)
                if nb >1:
                    c_var = "F" + str(int(n[0])) + "[" + str(((n[1]-1)*nb)+nb-1) + " : " + str((n[1]-1)*nb) + "]"
                else:

                    c_var = "F" + str(int(n[0])) + "[" + str(n[1]-1) + "]"
                # c_var = "F"+str(int(n[0])) + "[" + str(n[1]-1) + "]"
            elif ln ==3:
                index = ( (n[1]-1) * (part_size/parity_bits) ) + n[2]
                # print >> f2 , index
                c_var = "F"+str(int(n[0])) + "[" + str(index) + "]"
            # elif ln == 4: #write later
                # pass

        elif ln > 1 and str(n[0])  in parity_funcs and (not num.startswith("F")):
            # print >> f2 , "numbers only" , n
            if ln == 2:
                index = ( (n[0]-1) * (part_size/parity_bits) ) + n[1]
                # print >> f2 , index
                c_var =   str(index)
            elif ln ==3: #write later
                # print >> f2 , "Write the code now"
                replace_var = getbitvalues(num,-1)
                # print >> f2 , replace_var
                m = map(int, re.findall(r'\d+', replace_var))
                # print >> f2 , str(m[0]-n[1]) , str(m[0]-n[2])
                c_var = c_var =   str(m[0]-n[1]) + ":" + str(m[0]-n[2])
            # elif ln == 4: #write later
            #     pass
        elif ln == 1 and num.isdigit() :
            c_var = num
        else:
            c_var = str(num)

    else: # **************** Others
        # print >> f2 , "here"
        c_var = num

    # print >> f2 , "c_var ::" , c_var
    return c_var


###########################################################################################################




    # if len(n)> 1 and fn != "CMP" and ("x" + str(n[0])) not in parity_funcs:
    #     c_var = "F"+str(int(n[0])) + "[" + bits_dict[n[1]] + "]"
    #     return c_var
    # elif len(n) == 1 and fn!="CMP" and ("x" + str(n[0])) not in parity_funcs :
    #     c_var = bits_dict[n[0]]
    #     return c_var
    # elif fn =="CMP":
    #     c_var = "F"+str(int(n[0])) + "[" + str(n[1]-1) + "]"
    #     return c_var
    # elif (len(n) > 1 and ("x" + str(n[0]))  in parity_funcs)  :
    #     # print >> f2 , "Here 3"
    #     c_var = "F"+str(int(n[0])) + "[" + str(n[1]-1) + "]"
    #     return c_var
    # elif (len(n) == 1 and  (("x" + str(n[0]))  in parity_funcs)) :
    #     # print >> f2 , "Here 3"
    #     c_var = n[0] -1
    #     return c_var
    #
    #
    # else:
    #     return num

		# return  (str((int(num)*8)-1) + ":" + str((int(num)-1)*8))



def processVar(n):
    # f2 = open("./out/output.v" , 'a')
    # print >> f2 , "\nn" , n
    m = re.search('0x(.+)', n)
    o = re.search('(.+)_(.+)\w+' , n)
    # print >> f2 , "\no, " , o
    if m:
        # print >> f2 , m.group(1)
        return ("8'h" + m.group(1))
    elif o:
        byt = o.group(1)
        p = map(int, re.findall(r'\d+', n))
        bt = str(p[0])
        # print >> f2 , "\n Matching", o.group(1), o.group(2)
        ret_string = byt + "[" + bt + "]"
        # print >> f2 , ret_string
        return ret_string
    else:
        return n

def opHandler_func(f , tv , sv ,l):
    # f2 = open("./out/output.v" , 'a')
    # print >> f2 , "f" , f
    # print >> f2 , f
    global last_pop
    if f in ('XOR' , 'RS' , 'LS' , 'MUL' , 'AND'):
        a = tv.pop()
        b = tv.pop()
        c= tv.pop()
        sv.append(c)
        last_pop = c
        # print >> f2 , c ,"=" ,  processVar(b) , "^" , processVar(a) , ";"
        l.append(c + "=" +  processVar(b) + symbol_dict[f] + processVar(a) + ";")
    elif f in ('LKUP'):
        a = tv.pop()
        b = tv.pop()
        c= tv.pop()
        sv.append(c)
        last_pop = c
        l.append(c+ " = " + processVar(a) + "[" + processVar(b) + "]" + ";")


    else:
        # print >> f2 , so.new_function[f]
        if len(so.new_function[f]) == 1:
            # print >> f2 , "Here"
            a = tv.pop()
            b = tv.pop()
            sv.append(b)
            last_pop = b
            l.append(b + " = " + f + "( " + a + " ) ;")
        elif len(so.new_function[f]) == 2:
            # print >> f2 , "Here1"
            a = tv.pop()
            b = tv.pop()
            c = tv.pop()
            sv.append(c)
            last_pop = c
            l.append(c + " = " + f + "( " + a + " , " + b + " ) ;")



def writeNewFunction(funcList):
    f2 = open("./out/output.v" , 'a')
    nf =set()
    # print >> f2 , "Calling funclist"
    for i in tuple(funcList):
        # print >> f2 , so.function_depstack[i]
        c_var = ""
        for x in xrange(0, len(so.new_function[i])):
            if x == len(so.new_function[i]) -1:
                c_var = c_var + so.new_function[i][x]
            else:
                c_var = c_var + so.new_function[i][x] + ","
        # print >> f2 , "c_var" ,c_var
        print >> f2 , "\n\nfunction [7:0] " , i , ";"
        print >> f2 , "input [7:0] " , c_var , ";"
        temp_varholder =[]
        stored_val = []
        lines = []
        for j in so.function_depstack[i]:

            if j in so.defined_func or j in so.new_defined_func:
                # print >> f2 , "Here"
                if j not in funcList and j in so.new_defined_func:
                    nf.add(j)
                # print >> f2 , "j" , j
                opHandler_func(j,temp_varholder, stored_val, lines)
            else:
                # nz = processForOriginal(z)
                temp_varholder.append(j)

        s_var = ""
        for x in xrange(0, len(stored_val)):
            if x == len(stored_val) -1:
                s_var = s_var + stored_val[x]
            else:
                s_var = s_var + stored_val[x] + ","
        print >> f2 , "reg [7:0] " , s_var , " ;"
        print >> f2 , "begin"
        for y in lines:
            print >> f2 , y
        print >> f2 , i , "=" , last_pop , ";"
        print >> f2 , "end"
        print >> f2 , "endfunction\n\n"
    # print >> f2 , nf
    f2.close()
    return nf



#####################################################Write functions for main module#####################################





def writeforRounds(dep_lst , dep_var , f_lin):
    f2 = open("./out/output.v" , 'a')

    print >> f2 , "module Camellia_Core (din, dout, KEY, sel);"

    print >> f2 , "input  [127:0] din, KEY;"
    print >> f2 , "input          sel;"
    print >> f2 , "output [127:0] dout;\n"

    wires_128 = ""
    wires_16 = ""
    wires = ""

    for x in xrange(0, len(so.roundFunctions)):
        num = map(int, re.findall(r'\d+', so.roundFunctions[x]))

        so.roundFunctions[x]
        if x == len(so.roundFunctions) -1:
            if f_lin[1][num[0]] in ("FINDPARITY", "PREDICTPARITY" ,"CMP" ):
                wires_16 = wires_16 + so.roundFunctions[x]
            else:
                wires_128 = wires_128 + so.roundFunctions[x]
        else:
            if f_lin[1][num[0]] in ("FINDPARITY", "PREDICTPARITY" ,"CMP" ):
                wires_16 = wires_16 + so.roundFunctions[x] + " , "
            else:
                wires_128 = wires_128 + so.roundFunctions[x] + " , "

    print >> f2 , "wire [127:0] " ,wires_128 ,  " F0 ;"
    print >> f2 , "wire [" , (16*(part_size/parity_bits) -1) , ":0] " ,wires_16 ,  " ;\n\n"
    # for y in xrange(1,17):
    #     line =  "assign " + " F0[" +  str((y*8)-1) + ":" + str((y-1)*8) + "] = " + " din[" + str((y*8)-1)  + ":" + str((y-1)*8) + "]"
    #     print >> f2 , line
    line =  "assign " + " F0" +   " = " + " din ;"
    print >> f2 , line
    # f2.close()
    write_function_list = set()
    for x in dep_var:
        # print >> f2 , "x ", dep_var[x]
        # print >> f2 , "Prinitng length: ", len(dep_var[x])
        formBitdict(len(dep_var[x]))

        # print >> f2 , "Bitsdict", bits_dict
        # findDepfunc(x,dep_var, f_lin)
        # if f_lin[1][x] == "KEYXOR":

        print >> f2 , "\n"

        for y in dep_var[x]:
            temp_varholder = []

            if x in dep_lst.keys() and y in dep_lst[x].keys() :
                for z in dep_lst[x][y]:
                    # print >> f2 , "z" , z
                    if z in so.defined_func or z in so.new_defined_func:
                        if z in so.new_defined_func:
                            write_function_list.add(z)
                        opHandler(z , temp_varholder,f_lin[1][x] , y)
                    else:
                        # getbitvalues(z)
                        nz = processForOriginal(z , f_lin[1][x],x)
                        # print >> f2 , "Inserting ", z , nz
                        temp_varholder.append(nz)

                if f_lin[1][x] in ("KEYXOR", "DXOR"):
                    # print >> f2 , "Sending value from here"
                    store_val = "F" + str(x) + "[" + getbitvalues(y,len(dep_var[x])) + "]"
                    print >> f2 , "assign", store_val , "=" , temp_varholder[0] ,";"
                elif f_lin[1][x] in ("SUBBYTE"):
                    store_val = "F" + str(x) + "[" + getbitvalues(y,len(dep_var[x])) + "]"
                    instance_name = "SB" + str(y)
                    print >> f2 , "SubByte ", instance_name , "(" , temp_varholder[0] , "," , store_val , ");"
                # elif  f_lin[1][x] in ("SWAP"):
                #     store_val = "F" + str(x) + "[" + str(((17-y)*8)-1) + ":" +  str(((17-y)-1)*8) + "]"

                elif f_lin[1][x] in ("FINDPARITY", "PREDICTPARITY" ,"CMP" ):
                    parity_funcs.add(str(x))
                    nb = (part_size/parity_bits)
                    if nb >1:
                        store_val = "F" + str(x) + "[" + str(((y-1)*nb)+nb-1) + " : " + str((y-1)*nb) + "]"
                    else:

                        store_val = "F" + str(x) + "[" + str(y-1) + "]"
                    # if f_lin[1][x-1] == "SUBBYTE":
                    #     print >> f2 , "Here insert code"
                    #     instance_name = "PS" + str(y)
                    #     c_instance =  "ParitySbox " + instance_name + " ( " + temp_varholder[0] + " , X[ " + str(y) + " ];"
                    #     print >> f2 , c_instance
                    #     print >> f2 ,  "assign", store_val , "=" , temp_varholder[0] ,";"
                    print >> f2 , "assign", store_val , "=" , temp_varholder[0] ,";"
                else:
                    # print >> f2 , "Here"
                    store_val = "F" + str(x) + "[" + getbitvalues(y,len(dep_var[x])) + "]"
                    print >> f2 , "assign", store_val , "=" , temp_varholder[0] ,";"

            else:
                if f_lin[1][x] in ("FINDPARITY", "PREDICTPARITY" ,"CMP" ):
                    nb = (part_size/parity_bits)
                    if nb >1:
                        store_val = "F" + str(x) + "[" + str(((y-1)*nb)+nb-1) + " : " + str((y-1)*nb) + "]"
                    else:

                        store_val = "F" + str(x) + "[" + str(y-1) + "]"
                    # print >> f2 , dep_var[x][y][0]
                    load_val = processForOriginal(dep_var[x][y][0] , f_lin[1][x],x)
                    print >> f2 , "assign" , store_val , "=" , load_val ,";"
                else:
                    store_val = "F" + str(x) + "[" + getbitvalues(y,len(dep_var[x])) + "]"
                    load_val = processForOriginal(dep_var[x][y][0] , f_lin[1][x],x)
                    print >> f2 , "assign" , store_val , "=" , load_val ,";"

    # print >> f2 , "WritefunctionList" , write_function_list
    f2.close()
    if write_function_list:
        nf =writeNewFunction(write_function_list)
        if nf:
            writeNewFunction(nf)
    f2 = open("./out/output.v" , 'a')
    print >> f2 , "endmodule"
    f2.close()
