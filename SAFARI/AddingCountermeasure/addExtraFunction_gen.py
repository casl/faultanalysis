"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:

This code adds extra fucntins in the specifications which are required for the countermeasure
--------------------------------------------------------------------------------------------------
"""




import testing_multiparity as tm
pbits = 0 # number of parity bits used
fparity = set()


def addSboxParity():
    fp = open("./out/out_spec.bcs" , 'a')
    # print >> fp , "\t\t<func> < F_GENS ( a ) >"
    # print >> fp , "\t\t\t< \repeat ( i : F_EQ ( 256 ) ) >"
    # print >> fp , "\t\t\t< \repeat ( i : F_EQ ( 256 ) ) >"
    fp.close()

def addCompare():

    fp = open("./out/out_spec.bcs" , 'a')
    print >> fp , "\t\t<func> < F_ECMP ( a , b ) >"
    print >> fp , "\t\t\t< r : { (a , b ) : F_XOR ( a , b ) } >"
    print >> fp , "\t\t\t< RET : { r : F_IZ ( r ) } >"
    print >> fp , "\t\t</func>\n"
    fp.close()

def addCompare_mask():

    fp = open("./out/out_spec.bcs" , 'a')
    print >> fp , "\t\t<func> < F_CMPMASK ( a , b ) >"
    print >> fp , "\t\t\t< s1 : { (a , m1 ) : F_XOR ( a , m1 ) } >"
    print >> fp , "\t\t\t< x : { (s1 , m3 ) : F_GMUL ( s1 , m3 ) } >"
    print >> fp , "\t\t\t< s2 : { (b , m2 ) : F_XOR ( b , m2 ) } >"
    print >> fp , "\t\t\t< y : { (s2 , m3 ) : F_GMUL ( s2 , m3 ) } >"
    print >> fp , "\t\t\t< c : { (x , y ) : F_XOR ( x , y ) } >"
    print >> fp , "\t\t\t< d : { (m1 , m2 ) : F_XOR ( m1 , m2 ) } >"
    print >> fp , "\t\t\t< e : { (d , m3 ) : F_GMUL ( d , m3 ) } >"
    print >> fp , "\t\t\t< f : { (s1 , c ) : F_XOR ( s1 , c ) } >"
    print >> fp , "\t\t\t< g : { (f , e ) : F_XOR ( f , e ) } >"
    print >> fp , "\t\t\t< h : { (s2 , c ) : F_XOR ( s2 , c ) } >"
    print >> fp , "\t\t\t< i : { (h , e ) : F_XOR ( h , e ) } >"

    # print >> fp , "\t\t\t< r : { (a , b ) : F_XOR ( a , b ) } >"
    # print >> fp , "\t\t\t< RET : { r : F_IZ ( r ) } >"
    print >> fp , "\t\t</func>\n"
    fp.close()



def addBit():
    fp = open("./out/out_spec.bcs" , 'a')
    print >> fp , "\t\t<func> < F_BIT ( a , b ) >"
    print >> fp , "\t\t\t< c : { a : F_RS ( a , '0x3' ) } >"
    print >> fp , "\t\t\t< d : { (a , c) : F_LKUP ( c , b ) } >"
    print >> fp , "\t\t\t< e : { a : F_AND ( a , '0x7' ) } >"
    print >> fp , "\t\t\t< f : { e : F_LS ( e , '0x0' ) } >"
    print >> fp , "\t\t\t< g : { (d , f) : F_RS ( d , f ) } >"
    print >> fp , "\t\t\t< h : { g : F_AND ( g , '0x1' ) } >"
    print >> fp , "\t\t\tret h\n"
    print >> fp , "\t\t</func>\n"
    fp.close()




def addBit2():
    fp = open("./out/out_spec.bcs" , 'a')
    print >> fp , "\t\t<func> < F_BIT ( a , b ) >"
    print >> fp , "\t\t\t< c : { a : F_RS ( a , '0x2' ) } >"
    print >> fp , "\t\t\t< d : { (a , c) : F_LKUP ( c , b ) } >"
    print >> fp , "\t\t\t< e : { a : F_AND ( a , '0x3' ) } >"
    print >> fp , "\t\t\t< f : { e : F_LS ( e , '0x1' ) } >"
    print >> fp , "\t\t\t< g : { (d , f) : F_RS ( d , f ) } >"
    print >> fp , "\t\t\t< h : { g : F_AND ( g , '0x3' ) } >"
    print >> fp , "\t\t\tret h\n"
    print >> fp , "\t\t</func>\n"
    fp.close()

def addBit4():
    fp = open("./out/out_spec.bcs" , 'a')
    print >> fp , "\t\t<func> < F_BIT ( a , b ) >"
    print >> fp , "\t\t\t< c : { a : F_RS ( a , '0x1' ) } >"
    print >> fp , "\t\t\t< d : { (a , c) : F_LKUP ( c , b ) } >"
    print >> fp , "\t\t\t< e : { a : F_AND ( a , '0x1' ) } >"
    print >> fp , "\t\t\t< f : { e : F_LS ( e , '0x2' ) } >"
    print >> fp , "\t\t\t< g : { (d , f) : F_RS ( d , f ) } >"
    print >> fp , "\t\t\t< h : { g : F_AND ( g , '0xf' ) } >"
    print >> fp , "\t\t\tret h\n"
    print >> fp , "\t\t</func>\n"
    fp.close()

def addParity():
    fp = open("./out/out_spec.bcs" , 'a')
    if pbits == 1:
        addBit()
        print >> fp , "\t\t<func> < F_PARITY ( a ) >\n\
    		          < b : { a : F_RS ( a , 4 ) } >\n\
    		          < c : { ( a , b ) : F_XOR ( a , b ) } >\n\
    		          < d : {  c  : F_AND ( c , '0xf' ) } >\n\
    		          < e : {  d  : F_RS ( '0x6996' , d ) } >\n\
    		          < f : {  e  : F_AND ( e , '0x1' ) } >\n\
    		          ret f\n\
                    </func>\n\n"
    elif pbits == 2:
        addBit2()
        print >> fp , "\t\t<func> < F_PARITY ( a ) >\n\
    		          < b : { a : F_RS ( a , 4 ) } >\n\
    		          < c : { ( a , b ) : F_XOR ( a , b ) } >\n\
    		          < d : {  c  : F_AND ( c , '0xf' ) } >\n\
                        < e : {  d  : F_LS ( d , 1 ) } >\n\
    		          < f : {  e  : F_RS ( '0x1B4EB1E4' , e ) } >\n\
    		          < g : {  f  : F_AND ( f , '0x3' ) } >\n\
    		          ret g\n\
                    </func>\n\n"

    elif pbits == 4:
        addBit4()
        print >> fp , "\t\t<func> < F_PARITY ( a ) >\n\
    		          < b : { a : F_RS ( a , 4 ) } >\n\
    		          < c : { ( a , b ) : F_XOR ( a , b ) } >\n\
    		          < d : {  c  : F_AND ( c , '0xf' ) } >\n\
    		          ret d\n\
                    </func>\n\n"
    fp.close()



def addgenParityMDS(mds_matrix,index):
    global pbits
    fp = open("./out/out_spec.bcs" , 'a')
    fname = "F_GENPARITYMDS"#+ str(index) + str(j)
    print >> fp , "\t\t<func> < " ,  fname, " ( a , b , c , d , e , f , g , h ) >"
    for j in xrange (0,pbits):

        org = ['a' , 'b' , 'c' , 'd']
        parity = ['e' , 'f' , 'g' , 'h']
        for i in xrange (0,4):
            mult = mds_matrix[i][0]
            fparity.add(mult)
            # if mult == 1:
            #     fname = "F_CALCPARITY1"
            # else:
            fname = "F_CALCPARITY" + str(j) + str(mult)

            sv = "x" + str(i+1)
            print >> fp , "\t\t\t< " , sv , " : { ( " , org[i] , "," ,  parity[i] , " ) : " , fname ," ( "  , parity[i] ," , " , org[i], " ) }  >"
        print >> fp , "\t\t\t< x5 : { ( x1 , x2 ) : F_XOR ( x1 , x2 ) } >"
        print >> fp , "\t\t\t< x6 : { ( x3 , x4 ) : F_XOR ( x3 , x4 ) } >"

        if j!=0:
            print >> fp , "\t\t\t< x8 : { ( x5 , x6 ) : F_XOR ( x5 , x6 ) } >"
            print >> fp , "\t\t\t< x8 : { x8 : F_LS ( x8 ,", j, " ) } >"
            print >> fp , "\t\t\t< x7 : { (x7 , x8) : F_XOR ( x8 , x7 ) } >"
        else:
            print >> fp , "\t\t\t< x7 : { ( x5 , x6 ) : F_XOR ( x5 , x6 ) } >"
    print >> fp , "\t\t\t< x7 : { x7 : F_AND ( x7 ,",  (2**(j+1))-1 , ") } >"
    print >> fp , "\t\t\tret x7"
    print >> fp,  "\t\t</func>\n\n"
    fp.close()

def PredictParity (m):
    fp = open("./out/out_spec.bcs" , 'a')
    a = [1,2,3,4,5,6,7,8]
    for itr in xrange(0,pbits):
        a = [1,2,3,4,5,6,7,8]
        final_res = tm.calculateParity (a,m)
        # print "Result" , final_res
        if pbits ==1:
            b_list =  tm.checkForEightbits(final_res, itr)
        elif pbits==2:
            b_list =  tm.checkForFourbits(final_res, itr)
        elif pbits==4:
            b_list =  tm.checkForTwobits(final_res, itr)
        # print tm.checkForTwobits(final_res, itr)
        # print b_list
        var_list_xor = []

        for i in b_list:
            n = "a_["+ str(i-1) + "]"
            var_list_xor.append(n)
        var_list_xor.append('b')
        # print var_list_xor
        i = 1
        fname = "F_CALCPARITY" + str(itr) +  str(m)
        print >> fp, "\t\t<func> < " , fname , " ( a , b ) >"
        print >> fp , "\t\t\t < b : { b  : F_RS ( b ,", itr , " ) } >"
        print >> fp , "\t\t\t < b : { b  : F_AND(b , 1 ) } >"
        while  len(var_list_xor) >  1:

            x = var_list_xor.pop()
            y = var_list_xor.pop()
            z = "F_XOR ( " + x +" , " + y + ")"
            store_val = "n" + str(i)
            print >> fp, "\t\t\t< " , store_val , ": { ( " , x , " , " , y , " ) :" , z , " } >"
            var_list_xor.append(store_val)
            i=i+1
        print >> fp , "\t\t\tret " , store_val
        print >> fp , "\t\t</func>\n\n"
    fp.close()

def PredictParity1():
    fp = open("./out/out_spec.bcs" , 'a')
    for itr in xrange(0,pbits):
        fname = "F_CALCPARITY" + str(itr) + "1"
        print >> fp, "\t\t<func> < " , fname , " ( a , b ) >"
        print >> fp , "\t\t\t < b : { b  : F_RS ( b ,", itr , " ) } >"
        print >> fp , "\t\t\t < b : { b  : F_AND(b , 1 ) } >"
        print >> fp , "\t\t\tret b"
        print >> fp , "\t\t</func>\n\n"
    # print >> fp , "\t</operations>"
    fp.close()

def AddAllFunctions(mm):
    # print "Here"

    addCompare_mask()
    if pbits >0:
        addSboxParity()
        addParity()
        for x in xrange(0,len(mm)):
            # print "MDS", mm[x]
            addgenParityMDS(mm[x],x)
        # print "fparity" , fparity
        for x in fparity:
            if x ==1:
                PredictParity1()
            else:
                PredictParity(x)
    fp = open("./out/out_spec.bcs" , 'a')
    print >> fp , "\t</operations>"
    fp.close()
    # PredictParity(2)
    # PredictParity(4)
    # PredictParity(6)
    # PredictParity1()
