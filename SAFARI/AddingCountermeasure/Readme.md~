Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------


**Description of files**

test_spec.txt ----------------------------------------- Contains specification

main.py ----------------------------------------------- main file

parse_specification.py -------------------------------- Contains grammar of the specification, written in pyparsing

store_operands.py ---------------------------------------- Reads specification and store different elements from the specification in different data structures.

generate_redundant.py ------------------------------------ Add redundancy countermeasure to the specification

generate_parity.py --------------------------------------- Add 1 bit parity to 1 each byte in the countermeasure to the specification

generate_parity_4bits.py --------------------------------- Add 1 bit parity for each of the 4 bits in the countermeasure to the specification

generate_parity_2bits.py --------------------------------- Add 1 bit parity for each of the 2 bits in the countermeasure to the specification

generate_redundant_masking.py ---------------------------- Add masking countermeasure to the specification

copy_file.py --------------------------------------------- Copy initial part of the specification to the output specification

addExtraFunction_gen.py ---------------------------------- Add extra functions required for the Countermeasure





**Files need to be always added in the main:**

store_operands

parse_specification

copy_file

addExtraFunction_gen

**Files Needs to be added in the main based on countermeasure :**

generate_parity

generate_redundant

generate_parity_4bits

generate_parity_2bits

generate_redundant_masking

**How to execute**

change input specification file name in the main.py file

execute "python main.py"

output will be stored in "./out/out_spec.txt"
