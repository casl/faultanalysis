"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and / or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details: 
This code contains the grammer which will parse the BCS
--------------------------------------------------------------------------------------------------
"""


from pyparsing import *
import store_operands as so

def Syntax():

    var = Word(alphanums)
    st_var = Word(alphanums)                                                                                                    																														#Variables
    dec_fname =  "F_" + Word(alphanums) + Optional(OneOrMore( "_" + Word(alphanums)))
    fname =  "F_" + Word(alphanums)  + Optional(OneOrMore( "_" + Word(alphanums)))                                                                                           																												#function name
    constant = "'" + Word(alphanums) + "'"                                                                                      																												#constant (if passed in function)
    op = oneOf( ', :')                                                                                                             																															#separaor
    lpar  = Literal( '(' )                                                                                                   																																#left paranthesis
    rpar  = Literal( ')' )
    # var_list = "[ "  + Word(alphanums) + "]"                                                                                                    																															#right paranthesis
    var_as = Word(alphanums)
    var_bits = Word(alphanums) + "_[" + Word(alphanums) + "]"
    prototype = (dec_fname + lpar + (var_bits|var|constant).setParseAction(so.pushNewFuncVar) + ZeroOrMore("," + (var_bits|var|constant).setParseAction(so.pushNewFuncVar)) + rpar ).setParseAction(so.pushNewFunc)
    prototype_dep = (dec_fname + lpar + (var_bits|var|constant).setParseAction(so.pushFuncVariable) + ZeroOrMore("," + (var_bits|constant|var).setParseAction(so.pushFuncVariable)) + rpar ).setParseAction(so.pushLines)                                          																			#function prototype
    varused = Optional(lpar) + (var_bits|var) + ZeroOrMore("," + (var_bits|var)) + Optional(rpar) + ":" + prototype_dep                                   																			#functionality of function, how used
    line = ("<" + (st_var).setParseAction(so.pushStoredVar) + ":" + "{" + (var_bits| varused | var_as  ) + "}"  +  ">")                                                                        																											#each line of fucntion
    ret = "ret" + var
    exit = "< exit >"
    cond_func = oneOf ( 'F_EQ , F_NEQ , F_LE , F_GE , F_LT , F_GT')
    condition =  cond_func + lpar + var + rpar
    loops = "< repeat " + lpar + var + ":" + condition + rpar + ">" + OneOrMore(line) + "< /repeat >"
    ifelse = "< if " +lpar + var + ":" + condition + rpar + ">" + OneOrMore(line|ret|exit) + Optional ( "< else >" + OneOrMore(line|ret|exit)  ) + "< /if >"

                                                                                                      																										#return statement in each function
    function = "<func>" + " <" + prototype + ">" + ZeroOrMore(line|loops|ifelse) + Optional(ret|exit) + "</func>"                                 																					#each function
    operations = "<operations>"  + Optional (OneOrMore(function))  + "</operations>"
    values = Word(hexnums).setParseAction(so.AddLookupVariables)
    mem = ( Word(alphanums).setParseAction(so.AddLookup) + "[" + Word(nums) + "]" ) + Optional( "{" + OneOrMore(values) + "}" )                                                																							#operations
    memory = "<declaration>"  + OneOrMore(mem)  + "</declaration>"                                                                																							#memory variables used declaration
    linearity = oneOf("linear , nonlinear" )                                                                                       																												#if the function is linear/nonlinear
    rfname = (Word(alphanums) + Optional(OneOrMore( "_" + Word(alphanums)))).setParseAction(so.addRoundFunction)
    f_type = oneOf( 'KEYXOR , SUBBYTE , SWAP , MDS , DXOR , FXOR , CMP , PREDICTPARITY , FINDPARITY' , "ANDOP" )
    bit_string = Word(alphanums) + ":" + Word(alphanums)                                                                                              																												#roundfunction name e.g. F1, F2 etc
    rfunc = ("<" + rfname + ">" + "<" + linearity.setParseAction(so.PushLinearity) + ">" + "<" + f_type.setParseAction(so.PushType) + ">").setParseAction(so.increment_fno)
    prtname = (Word ( alphanums)  + Optional("[" + Word(nums)  + "]") + Optional("_[" + (bit_string|Word(alphanums)) + "]"))
    assg_ptname = (Word ( alphanums)  + Optional("[" + Word(nums)  + "]") + Optional("_[" + (bit_string|Word(alphanums)) + "]"))                                                   																							#start of roundfunction (1st line)
    pname = (Word ( alphanums)  + Optional("[" + Word(nums)  + "]") + Optional("_[" + (bit_string|Word(alphanums)) + "]"))
    # pname_bits = (Word ( alphanums)  + Optional("[" + Word(nums)  + "]"))                                                                 																							#part name e.g. F1[1], F1[2] etc
    depfunc = Forward()
    depfunc <<  ( fname + lpar + (depfunc|pname.setParseAction(so.pushVariable)) +
                        ZeroOrMore( op  + (depfunc|(pname))) + rpar ).setParseAction(so.pushFuctions)         																								 #dependency function of each part
    pused = Optional(lpar) + prtname.setParseAction(so.pushDepVar) + ZeroOrMore("," + prtname) + Optional(rpar) +  Optional(":" + depfunc)                            									#whole functionality of dependence
    part = ("<" + assg_ptname + ":" + "{" + pused + "}"  +  ">" ) .setParseAction(so.increment_pno)                                                                          															#each line of roundfunction i.e. each part described
    roundfunction = rfunc + "<" + OneOrMore(part) + "/>"                                                                              																									#each round function of cipher
    cipher = OneOrMore(roundfunction)                                                                                           																												 #whole cipher
    specification = "< begin >" + memory + operations + cipher + "< end >"                                                       																							#specification

    return specification
