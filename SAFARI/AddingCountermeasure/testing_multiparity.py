"""
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and / or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author : Indrani Roy
Email id : indrani.roy9999@gmail.com
Details:


--------------------------------------------------------------------------------------------------
"""




import math
import re
import main as mf

# a = [1,2,3,4,5,6,7,8]

powers = []


poly = ('{0:08b}'.format((int(mf.irr_poly,0))))

def shift_left(ab,x):
	# print "ab" , ab
	for i  in xrange(0,x):
		num = ab[len(ab)-1]
		# print "num", num
		del ab[len(ab)-1]
		b = 0

		ab.insert(0,b)
		return num



def form_xor(l1,l2):
	# print "len(l1)" , len(l1)
	# print "len(l2)" , len(l2)
	new_num = []*8
	# print "Printing xor"
	for i in xrange(0,8):
		# print str(l1[i])+"," +str(l2[i])
		new_num.append(str(l1[i])+"," +str(l2[i]))
	return new_num

def formAddingterm(num):
	add =[]
	# print "Num" , num
	for j in xrange(7,-1,-1):
		if int(poly[j]) == 1:
			add.append(num)
		else:
			add.append(0)
	# print "Add" ,add
	return add


def is_power2(num):
	return num != 0 and ((num & (num - 1)) == 0)


def FindPowers(c):
    if is_power2(c):
        # print "MATCH"
        x = int(math.log(c,2))
        powers.append(x)
    else:
        rem = c
        # print "You have to write a big code!!"
        while(rem != 0):
            p = int(math.log(c,2))
            rem = c - (2**p)
            c=rem
            powers.append(p)



def calforPower2(an, itr):
	# print "an" , an , itr
	n = an

	for i in xrange(0,itr):
		# print "Printing a start" , a
		# print "n" , n
		num = shift_left(n,1)
		# print "num" , num
		add = formAddingterm(num)
		# print  "add" ,add
		n = form_xor(n,add)
		# print "Printing a end" , a
	# print "n" , n , an
	return n


res = [0] * 8

def calculateParity(a , coeff):
	del powers[:]
	global res
	res = [0] * 8

	FindPowers(coeff)
	# print "Powers" , powers
	# print "Res" , res
	while powers:
		a = [1,2,3,4,5,6,7,8]
		# print "Printing A" , a
		i = powers.pop()
		# print "i = ", i
		if i ==0:
			# print "Here 1"
			res = form_xor(res, a)
		else:
			# print "Here 2"
			nv = a
			res = form_xor(res, calforPower2(nv,i))
			# print "Res " ,res
        # print "Result ", res
        return res




def formDict(bitarray,bit_list):
	# print "Starting"
	num_dict = dict()
	for i in bitarray:
		# print "Printing i: ", i
		n = map(int, re.findall(r'\d+', bit_list[i-1]))
		for j in xrange(0,len(n)):
			# print n[j]
			if n[j] in num_dict:
				num_dict[n[j]] = num_dict[n[j]] +1
			else:
				num_dict[n[j]] = 1


	# for z in num_dict:
	# 	print z ,(num_dict[z])%2

	return num_dict

def matchParity(NumDict , actual):
	xor_bitlist = []
	# print "Checking"
	del NumDict[0]
	for i in xrange (0, len(actual)):
		# print "actual" ,actual[i]
		if actual[i] in NumDict:
			# print "Match" , NumDict[actual[i]]
			if  int(NumDict[actual[i]])%2 == 0:
				# print "Here 1"
				xor_bitlist.append(actual[i])
				# print actual[i]
				del NumDict[actual[i]]
			elif int(NumDict[actual[i]])%2 == 1:
				# print "Here 2"

				del NumDict[actual[i]]
		else :
			# print "Here 3"
			xor_bitlist.append(actual[i])
			# print actual[i]


	for x in NumDict:
		if (NumDict[x] %2)== 1:
		# print x
			xor_bitlist.append(x)
	#print xor_bitlist

	return xor_bitlist



def checkForEightbits(bit_list, side):
	del powers[:]
	actual = [1,2,3,4,5,6,7,8]
	bitarr = [1,2,3,4,5,6,7,8]

	return  matchParity (formDict(bitarr,bit_list) , actual)

	# if side == 'l':
	# 	print "1st half"
	# 	# formDict(0,4,bit_list)
	# 	return  matchParity (formDict(0,4,bit_list) , actual_l)
	# elif side == 'r':
	# 	print "2nd half"
	# 	# formDict(4,8,bit_list)
	# 	return matchParity (formDict(4,8,bit_list) , actual_r)

def checkForFourbits(bit_list, side):
	del powers[:]
	actual_l = [1,5,3,7]
	actual_r = [2,6,4,8]

	if side == 0:
		# print "1st half"
		# formDict(0,4,bit_list)
		return  matchParity (formDict(actual_l,bit_list) , actual_l)
	elif side == 1:
		# print "2nd half"
		# formDict(4,8,bit_list)
		return matchParity (formDict(actual_r,bit_list) , actual_r)



def checkForTwobits(bit_list, side):
	del powers[:]
	actual_0 = [1,5]
	actual_1 = [2,6]
	actual_2 = [3,7]
	actual_3 = [4,8]

	if side == 0:
		# print "1st half"
		# formDict(0,4,bit_list)
		return  matchParity (formDict(actual_0,bit_list) , actual_0)
	elif side == 1:
		# print "2nd half"
		# formDict(0,4,bit_list)
		return  matchParity (formDict(actual_1,bit_list) , actual_1)
	elif side == 2:
		# print "3rd half"
		# formDict(0,4,bit_list)
		return  matchParity (formDict(actual_2,bit_list) , actual_2)
	elif side == 3:
		# print "4th half"
		# formDict(0,4,bit_list)
		return  matchParity (formDict(actual_3,bit_list) , actual_3)

# final_res = calculateParity (a,3)
# num_dict = dict ()

# print "Printing"
# for i in final_res:
# 	n = map(int, re.findall(r'\d+', i))
# 	for j in xrange(0,len(n)):
# 		print n[j]
# 		if n[j] in num_dict:
# 			num_dict[n[j]] = num_dict[n[j]] +1
# 		else:
# 			num_dict[n[j]] = 1


# for z in num_dict:
# 	print z ,(num_dict[z])%2



# checkForFourbits(final_res)

# for x in xrange(0,3):

# num = shift_left(a,1)
# add = formAddingterm(num)
# form_xor(a,add)
# num = shift_left(a,1)
# add = formAddingterm(num)
# form_xor(a,add)

# # for i in xrange(0,len(add)):
# # 	print add[i]
