Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

//Remove this line and above copyright before executing the code



< begin >

	<declaration>

		PKEY[16]
		PSBOX[256]
		SBOX[256] {  63    7c    77    7b    f2    6b    6f    c5    30    01    67    2b    fe    d7    ab    76     ca    82    c9    7d    fa    59    47    f0    ad    d4    a2    af    9c    a4    72    c0     b7    fd    93    26    36    3f    f7    cc    34    a5    e5    f1    71    d8    31    15     04    c7    23   c3   18   96   05   9a   07   12   80   e2   eb   27   b2   75    09   83   2c   1a   1b   6e   5a   a0   52   3b   d6   b3   29   e3   2f   84    53   d1   00   ed   20   fc   b1   5b   6a   cb   be   39   4a   4c   58   cf    d0   ef   aa   fb   43   4d   33   85   45   f9   02   7f   50   3c   9f   a8    51   a3   40   8f   92   9d   38   f5   bc   b6   da   21   10   ff   f3   d2    cd   0c   13   ec   5f   97   44   17   c4   a7   7e   3d   64   5d   19   73    60   81   4f   dc   22   2a   90   88   46   ee   b8   14   de   5e   0b   db    e0   32   3a   0a   49   06   24   5c   c2   d3   ac   62   91   95   e4   79    e7   c8   37   6d   8d   d5   4e   a9   6c   56   f4   ea   65   7a   ae   08    ba   78   25   2e   1c   a6   b4   c6   e8   dd   74   1f   4b   bd   8b   8a    70   3e   b5   66   48   03   f6   0e   61   35   57   b9   86   c1   1d   9e    e1   f8   98   11   69   d9   8e   94   9b   1e   87   e9   ce   55   28   df    8c   a1   89   0d   bf   e6   42   68   41   99   2d   0f   b0   54   bb   16}
		KEY[16] {2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c}

	</declaration>

	<operations>

		 <func> < F_MUL2 ( a ) >
			< h : { a : F_RS ( a , 7 ) } >
			< t : { a : F_LS ( a , 1 ) } >
			< n : { h : F_MUL ( h , '0x1b' ) } >
			< m : { ( n , t ) : F_XOR ( n , t ) } >
			ret m
		</func>

		<func> < F_MUL3 ( a ) >
			< x : { a : F_MUL2 ( a ) } >
			< t : { ( x , a ) : F_XOR ( a , x ) } >
			ret t
		</func>

		<func> < F_PARITY ( a ) >
		          < b : { a : F_RS ( a , 4 ) } >
		          < c : { ( a , b ) : F_XOR ( a , b ) } >
		          < d : {  c  : F_AND ( c , '0xf' ) } >
		          < e : {  d  : F_RS ( '0x6996' , d ) } >
		          < f : {  e  : F_AND ( e , '0x1' ) } >
		          ret f
                </func>


		<func> < F_ECMP ( a , b ) >
			< r : { (a , b ) : F_XOR ( a , b ) } >
			< RET : { r : F_IZ ( r ) } >
		</func>

		<func> <  F_GENPARITYMDS0  ( a , b , c , d , e , f , g , h ) >
			<  x1  : { (  a , e  ) :  F_CALCPARITY02  (  e  ,  a  ) }  >
			<  x2  : { (  b , f  ) :  F_CALCPARITY03  (  f  ,  b  ) }  >
			<  x3  : { (  c , g  ) :  F_CALCPARITY1  (  g  ,  c  ) }  >
			<  x4  : { (  d , h  ) :  F_CALCPARITY1  (  h  ,  d  ) }  >
			< x5 : { ( x1 , x2 ) : F_XOR ( x1 , x2 ) } >
			< x6 : { ( x3 , x4 ) : F_XOR ( x3 , x4 ) } >
			< x7 : { ( x5 , x6 ) : F_XOR ( x5 , x6 ) } >
			ret x7
		</func>


		<func> <  F_CALCPARITY02  ( a , b ) >
			<  n1 : { (  b  ,  a_[7]  ) : F_XOR ( b , a_[7])  } >
			ret  n1
		</func>


		<func> <  F_CALCPARITY03  ( a , b ) >
			<  n1 : { (  b  ,  a_[6]  ) : F_XOR ( b , a_[6])  } >
			<  n2 : { (  n1  ,  a_[5]  ) : F_XOR ( n1 , a_[5])  } >
			<  n3 : { (  n2  ,  a_[4]  ) : F_XOR ( n2 , a_[4])  } >
			<  n4 : { (  n3  ,  a_[3]  ) : F_XOR ( n3 , a_[3])  } >
			<  n5 : { (  n4  ,  a_[2]  ) : F_XOR ( n4 , a_[2])  } >
			<  n6 : { (  n5  ,  a_[1]  ) : F_XOR ( n5 , a_[1])  } >
			<  n7 : { (  n6  ,  a_[0]  ) : F_XOR ( n6 , a_[0])  } >
			ret  n7
		</func>


		<func> <  F_CALCPARITY1  ( a , b ) >
			ret b
		</func>


	</operations>

< F1 > < linear > < KEYXOR > <
	<  F1[1]  : {  ( F0[1] )  :  F_XOR ( F0[1] , F_LKUP ( 1 , KEY ) )  } >
	<  F1[2]  : {  ( F0[2] )  :  F_XOR ( F0[2] , F_LKUP ( 2 , KEY ) )  } >
	<  F1[3]  : {  ( F0[3] )  :  F_XOR ( F0[3] , F_LKUP ( 3 , KEY ) )  } >
	<  F1[4]  : {  ( F0[4] )  :  F_XOR ( F0[4] , F_LKUP ( 4 , KEY ) )  } >
	<  F1[5]  : {  ( F0[5] )  :  F_XOR ( F0[5] , F_LKUP ( 5 , KEY ) )  } >
	<  F1[6]  : {  ( F0[6] )  :  F_XOR ( F0[6] , F_LKUP ( 6 , KEY ) )  } >
	<  F1[7]  : {  ( F0[7] )  :  F_XOR ( F0[7] , F_LKUP ( 7 , KEY ) )  } >
	<  F1[8]  : {  ( F0[8] )  :  F_XOR ( F0[8] , F_LKUP ( 8 , KEY ) )  } >
	<  F1[9]  : {  ( F0[9] )  :  F_XOR ( F0[9] , F_LKUP ( 9 , KEY ) )  } >
	<  F1[10]  : {  ( F0[10] )  :  F_XOR ( F0[10] , F_LKUP ( 10 , KEY ) )  } >
	<  F1[11]  : {  ( F0[11] )  :  F_XOR ( F0[11] , F_LKUP ( 11 , KEY ) )  } >
	<  F1[12]  : {  ( F0[12] )  :  F_XOR ( F0[12] , F_LKUP ( 12 , KEY ) )  } >
	<  F1[13]  : {  ( F0[13] )  :  F_XOR ( F0[13] , F_LKUP ( 13 , KEY ) )  } >
	<  F1[14]  : {  ( F0[14] )  :  F_XOR ( F0[14] , F_LKUP ( 14 , KEY ) )  } >
	<  F1[15]  : {  ( F0[15] )  :  F_XOR ( F0[15] , F_LKUP ( 15 , KEY ) )  } >
	<  F1[16]  : {  ( F0[16] )  :  F_XOR ( F0[16] , F_LKUP ( 16 , KEY ) )  } >

/>

< F2 > < linear > < FINDPARITY > <
	<  F2[1]  : {  ( F0[1] )  : F_PARITY ( F0[1] )  } >
	<  F2[2]  : {  ( F0[2] )  : F_PARITY ( F0[2] )  } >
	<  F2[3]  : {  ( F0[3] )  : F_PARITY ( F0[3] )  } >
	<  F2[4]  : {  ( F0[4] )  : F_PARITY ( F0[4] )  } >
	<  F2[5]  : {  ( F0[5] )  : F_PARITY ( F0[5] )  } >
	<  F2[6]  : {  ( F0[6] )  : F_PARITY ( F0[6] )  } >
	<  F2[7]  : {  ( F0[7] )  : F_PARITY ( F0[7] )  } >
	<  F2[8]  : {  ( F0[8] )  : F_PARITY ( F0[8] )  } >
	<  F2[9]  : {  ( F0[9] )  : F_PARITY ( F0[9] )  } >
	<  F2[10]  : {  ( F0[10] )  : F_PARITY ( F0[10] )  } >
	<  F2[11]  : {  ( F0[11] )  : F_PARITY ( F0[11] )  } >
	<  F2[12]  : {  ( F0[12] )  : F_PARITY ( F0[12] )  } >
	<  F2[13]  : {  ( F0[13] )  : F_PARITY ( F0[13] )  } >
	<  F2[14]  : {  ( F0[14] )  : F_PARITY ( F0[14] )  } >
	<  F2[15]  : {  ( F0[15] )  : F_PARITY ( F0[15] )  } >
	<  F2[16]  : {  ( F0[16] )  : F_PARITY ( F0[16] )  } >
/>

< F3 > < linear > < PREDICTPARITY > <
	<  F3[1]  : {  ( F2[1]  ) : F_XOR ( F2[1] , F_LKUP ( 1 , PKEY ) )  } >
	<  F3[2]  : {  ( F2[2]  ) : F_XOR ( F2[2] , F_LKUP ( 2 , PKEY ) )  } >
	<  F3[3]  : {  ( F2[3]  ) : F_XOR ( F2[3] , F_LKUP ( 3 , PKEY ) )  } >
	<  F3[4]  : {  ( F2[4]  ) : F_XOR ( F2[4] , F_LKUP ( 4 , PKEY ) )  } >
	<  F3[5]  : {  ( F2[5]  ) : F_XOR ( F2[5] , F_LKUP ( 5 , PKEY ) )  } >
	<  F3[6]  : {  ( F2[6]  ) : F_XOR ( F2[6] , F_LKUP ( 6 , PKEY ) )  } >
	<  F3[7]  : {  ( F2[7]  ) : F_XOR ( F2[7] , F_LKUP ( 7 , PKEY ) )  } >
	<  F3[8]  : {  ( F2[8]  ) : F_XOR ( F2[8] , F_LKUP ( 8 , PKEY ) )  } >
	<  F3[9]  : {  ( F2[9]  ) : F_XOR ( F2[9] , F_LKUP ( 9 , PKEY ) )  } >
	<  F3[10]  : {  ( F2[10]  ) : F_XOR ( F2[10] , F_LKUP ( 10 , PKEY ) )  } >
	<  F3[11]  : {  ( F2[11]  ) : F_XOR ( F2[11] , F_LKUP ( 11 , PKEY ) )  } >
	<  F3[12]  : {  ( F2[12]  ) : F_XOR ( F2[12] , F_LKUP ( 12 , PKEY ) )  } >
	<  F3[13]  : {  ( F2[13]  ) : F_XOR ( F2[13] , F_LKUP ( 13 , PKEY ) )  } >
	<  F3[14]  : {  ( F2[14]  ) : F_XOR ( F2[14] , F_LKUP ( 14 , PKEY ) )  } >
	<  F3[15]  : {  ( F2[15]  ) : F_XOR ( F2[15] , F_LKUP ( 15 , PKEY ) )  } >
	<  F3[16]  : {  ( F2[16]  ) : F_XOR ( F2[16] , F_LKUP ( 16 , PKEY ) )  } >
/>

< F4 > < linear > < FINDPARITY > <
	<  F4[1]  : {  ( F1[1] )  : F_PARITY ( F1[1] )  } >
	<  F4[2]  : {  ( F1[2] )  : F_PARITY ( F1[2] )  } >
	<  F4[3]  : {  ( F1[3] )  : F_PARITY ( F1[3] )  } >
	<  F4[4]  : {  ( F1[4] )  : F_PARITY ( F1[4] )  } >
	<  F4[5]  : {  ( F1[5] )  : F_PARITY ( F1[5] )  } >
	<  F4[6]  : {  ( F1[6] )  : F_PARITY ( F1[6] )  } >
	<  F4[7]  : {  ( F1[7] )  : F_PARITY ( F1[7] )  } >
	<  F4[8]  : {  ( F1[8] )  : F_PARITY ( F1[8] )  } >
	<  F4[9]  : {  ( F1[9] )  : F_PARITY ( F1[9] )  } >
	<  F4[10]  : {  ( F1[10] )  : F_PARITY ( F1[10] )  } >
	<  F4[11]  : {  ( F1[11] )  : F_PARITY ( F1[11] )  } >
	<  F4[12]  : {  ( F1[12] )  : F_PARITY ( F1[12] )  } >
	<  F4[13]  : {  ( F1[13] )  : F_PARITY ( F1[13] )  } >
	<  F4[14]  : {  ( F1[14] )  : F_PARITY ( F1[14] )  } >
	<  F4[15]  : {  ( F1[15] )  : F_PARITY ( F1[15] )  } >
	<  F4[16]  : {  ( F1[16] )  : F_PARITY ( F1[16] )  } >
/>

< F5 > <  linear > < CMP > <
	<  F5[1]  : {  ( F3[1] , F4[1] )  : F_ECMP ( F3[1] , F4[1] )  } >
	<  F5[2]  : {  ( F3[2] , F4[2] )  : F_ECMP ( F3[2] , F4[2] )  } >
	<  F5[3]  : {  ( F3[3] , F4[3] )  : F_ECMP ( F3[3] , F4[3] )  } >
	<  F5[4]  : {  ( F3[4] , F4[4] )  : F_ECMP ( F3[4] , F4[4] )  } >
	<  F5[5]  : {  ( F3[5] , F4[5] )  : F_ECMP ( F3[5] , F4[5] )  } >
	<  F5[6]  : {  ( F3[6] , F4[6] )  : F_ECMP ( F3[6] , F4[6] )  } >
	<  F5[7]  : {  ( F3[7] , F4[7] )  : F_ECMP ( F3[7] , F4[7] )  } >
	<  F5[8]  : {  ( F3[8] , F4[8] )  : F_ECMP ( F3[8] , F4[8] )  } >
	<  F5[9]  : {  ( F3[9] , F4[9] )  : F_ECMP ( F3[9] , F4[9] )  } >
	<  F5[10]  : {  ( F3[10] , F4[10] )  : F_ECMP ( F3[10] , F4[10] )  } >
	<  F5[11]  : {  ( F3[11] , F4[11] )  : F_ECMP ( F3[11] , F4[11] )  } >
	<  F5[12]  : {  ( F3[12] , F4[12] )  : F_ECMP ( F3[12] , F4[12] )  } >
	<  F5[13]  : {  ( F3[13] , F4[13] )  : F_ECMP ( F3[13] , F4[13] )  } >
	<  F5[14]  : {  ( F3[14] , F4[14] )  : F_ECMP ( F3[14] , F4[14] )  } >
	<  F5[15]  : {  ( F3[15] , F4[15] )  : F_ECMP ( F3[15] , F4[15] )  } >
	<  F5[16]  : {  ( F3[16] , F4[16] )  : F_ECMP ( F3[16] , F4[16] )  } >
/>

< F6 > < nonlinear > < SUBBYTE > <
	<  F6[1]  : {  ( F1[1] )  :  F_LKUP ( F1[1] , SBOX )  } >
	<  F6[2]  : {  ( F1[2] )  :  F_LKUP ( F1[2] , SBOX )  } >
	<  F6[3]  : {  ( F1[3] )  :  F_LKUP ( F1[3] , SBOX )  } >
	<  F6[4]  : {  ( F1[4] )  :  F_LKUP ( F1[4] , SBOX )  } >
	<  F6[5]  : {  ( F1[5] )  :  F_LKUP ( F1[5] , SBOX )  } >
	<  F6[6]  : {  ( F1[6] )  :  F_LKUP ( F1[6] , SBOX )  } >
	<  F6[7]  : {  ( F1[7] )  :  F_LKUP ( F1[7] , SBOX )  } >
	<  F6[8]  : {  ( F1[8] )  :  F_LKUP ( F1[8] , SBOX )  } >
	<  F6[9]  : {  ( F1[9] )  :  F_LKUP ( F1[9] , SBOX )  } >
	<  F6[10]  : {  ( F1[10] )  :  F_LKUP ( F1[10] , SBOX )  } >
	<  F6[11]  : {  ( F1[11] )  :  F_LKUP ( F1[11] , SBOX )  } >
	<  F6[12]  : {  ( F1[12] )  :  F_LKUP ( F1[12] , SBOX )  } >
	<  F6[13]  : {  ( F1[13] )  :  F_LKUP ( F1[13] , SBOX )  } >
	<  F6[14]  : {  ( F1[14] )  :  F_LKUP ( F1[14] , SBOX )  } >
	<  F6[15]  : {  ( F1[15] )  :  F_LKUP ( F1[15] , SBOX )  } >
	<  F6[16]  : {  ( F1[16] )  :  F_LKUP ( F1[16] , SBOX )  } >

/>

< F7 > < linear > < PREDICTPARITY > <
	<  F7[1]  : {  ( F4[1] , F1[1] ) : F_XOR (  F4[1] , F_LKUP ( F1[1] , PSBOX ) ) } >
	<  F7[2]  : {  ( F4[2] , F1[2] ) : F_XOR (  F4[2] , F_LKUP ( F1[2] , PSBOX ) ) } >
	<  F7[3]  : {  ( F4[3] , F1[3] ) : F_XOR (  F4[3] , F_LKUP ( F1[3] , PSBOX ) ) } >
	<  F7[4]  : {  ( F4[4] , F1[4] ) : F_XOR (  F4[4] , F_LKUP ( F1[4] , PSBOX ) ) } >
	<  F7[5]  : {  ( F4[5] , F1[5] ) : F_XOR (  F4[5] , F_LKUP ( F1[5] , PSBOX ) ) } >
	<  F7[6]  : {  ( F4[6] , F1[6] ) : F_XOR (  F4[6] , F_LKUP ( F1[6] , PSBOX ) ) } >
	<  F7[7]  : {  ( F4[7] , F1[7] ) : F_XOR (  F4[7] , F_LKUP ( F1[7] , PSBOX ) ) } >
	<  F7[8]  : {  ( F4[8] , F1[8] ) : F_XOR (  F4[8] , F_LKUP ( F1[8] , PSBOX ) ) } >
	<  F7[9]  : {  ( F4[9] , F1[9] ) : F_XOR (  F4[9] , F_LKUP ( F1[9] , PSBOX ) ) } >
	<  F7[10]  : {  ( F4[10] , F1[10] ) : F_XOR (  F4[10] , F_LKUP ( F1[10] , PSBOX ) ) } >
	<  F7[11]  : {  ( F4[11] , F1[11] ) : F_XOR (  F4[11] , F_LKUP ( F1[11] , PSBOX ) ) } >
	<  F7[12]  : {  ( F4[12] , F1[12] ) : F_XOR (  F4[12] , F_LKUP ( F1[12] , PSBOX ) ) } >
	<  F7[13]  : {  ( F4[13] , F1[13] ) : F_XOR (  F4[13] , F_LKUP ( F1[13] , PSBOX ) ) } >
	<  F7[14]  : {  ( F4[14] , F1[14] ) : F_XOR (  F4[14] , F_LKUP ( F1[14] , PSBOX ) ) } >
	<  F7[15]  : {  ( F4[15] , F1[15] ) : F_XOR (  F4[15] , F_LKUP ( F1[15] , PSBOX ) ) } >
	<  F7[16]  : {  ( F4[16] , F1[16] ) : F_XOR (  F4[16] , F_LKUP ( F1[16] , PSBOX ) ) } >
/>

< F8 > < linear > < FINDPARITY > <
	<  F8[1]  : {  ( F6[1] )  : F_PARITY ( F6[1] )  } >
	<  F8[2]  : {  ( F6[2] )  : F_PARITY ( F6[2] )  } >
	<  F8[3]  : {  ( F6[3] )  : F_PARITY ( F6[3] )  } >
	<  F8[4]  : {  ( F6[4] )  : F_PARITY ( F6[4] )  } >
	<  F8[5]  : {  ( F6[5] )  : F_PARITY ( F6[5] )  } >
	<  F8[6]  : {  ( F6[6] )  : F_PARITY ( F6[6] )  } >
	<  F8[7]  : {  ( F6[7] )  : F_PARITY ( F6[7] )  } >
	<  F8[8]  : {  ( F6[8] )  : F_PARITY ( F6[8] )  } >
	<  F8[9]  : {  ( F6[9] )  : F_PARITY ( F6[9] )  } >
	<  F8[10]  : {  ( F6[10] )  : F_PARITY ( F6[10] )  } >
	<  F8[11]  : {  ( F6[11] )  : F_PARITY ( F6[11] )  } >
	<  F8[12]  : {  ( F6[12] )  : F_PARITY ( F6[12] )  } >
	<  F8[13]  : {  ( F6[13] )  : F_PARITY ( F6[13] )  } >
	<  F8[14]  : {  ( F6[14] )  : F_PARITY ( F6[14] )  } >
	<  F8[15]  : {  ( F6[15] )  : F_PARITY ( F6[15] )  } >
	<  F8[16]  : {  ( F6[16] )  : F_PARITY ( F6[16] )  } >
/>

< F9 > <  linear > < CMP > <
	<  F9[1]  : {  ( F7[1] , F8[1] )  : F_ECMP ( F7[1] , F8[1] )  } >
	<  F9[2]  : {  ( F7[2] , F8[2] )  : F_ECMP ( F7[2] , F8[2] )  } >
	<  F9[3]  : {  ( F7[3] , F8[3] )  : F_ECMP ( F7[3] , F8[3] )  } >
	<  F9[4]  : {  ( F7[4] , F8[4] )  : F_ECMP ( F7[4] , F8[4] )  } >
	<  F9[5]  : {  ( F7[5] , F8[5] )  : F_ECMP ( F7[5] , F8[5] )  } >
	<  F9[6]  : {  ( F7[6] , F8[6] )  : F_ECMP ( F7[6] , F8[6] )  } >
	<  F9[7]  : {  ( F7[7] , F8[7] )  : F_ECMP ( F7[7] , F8[7] )  } >
	<  F9[8]  : {  ( F7[8] , F8[8] )  : F_ECMP ( F7[8] , F8[8] )  } >
	<  F9[9]  : {  ( F7[9] , F8[9] )  : F_ECMP ( F7[9] , F8[9] )  } >
	<  F9[10]  : {  ( F7[10] , F8[10] )  : F_ECMP ( F7[10] , F8[10] )  } >
	<  F9[11]  : {  ( F7[11] , F8[11] )  : F_ECMP ( F7[11] , F8[11] )  } >
	<  F9[12]  : {  ( F7[12] , F8[12] )  : F_ECMP ( F7[12] , F8[12] )  } >
	<  F9[13]  : {  ( F7[13] , F8[13] )  : F_ECMP ( F7[13] , F8[13] )  } >
	<  F9[14]  : {  ( F7[14] , F8[14] )  : F_ECMP ( F7[14] , F8[14] )  } >
	<  F9[15]  : {  ( F7[15] , F8[15] )  : F_ECMP ( F7[15] , F8[15] )  } >
	<  F9[16]  : {  ( F7[16] , F8[16] )  : F_ECMP ( F7[16] , F8[16] )  } >
/>

< F10 > < linear > < SWAP > <
	<  F10[1]  : {  ( F6[1] )  } >
	<  F10[2]  : {  ( F6[6] )  } >
	<  F10[3]  : {  ( F6[11] )  } >
	<  F10[4]  : {  ( F6[16] )  } >
	<  F10[5]  : {  ( F6[5] )  } >
	<  F10[6]  : {  ( F6[10] )  } >
	<  F10[7]  : {  ( F6[15] )  } >
	<  F10[8]  : {  ( F6[4] )  } >
	<  F10[9]  : {  ( F6[9] )  } >
	<  F10[10]  : {  ( F6[14] )  } >
	<  F10[11]  : {  ( F6[3] )  } >
	<  F10[12]  : {  ( F6[8] )  } >
	<  F10[13]  : {  ( F6[13] )  } >
	<  F10[14]  : {  ( F6[2] )  } >
	<  F10[15]  : {  ( F6[7] )  } >
	<  F10[16]  : {  ( F6[12] )  } >

/>

< F11 > < linear > < PREDICTPARITY > <
	<  F11[1]  : {  F8[1]  } >
	<  F11[2]  : {  F8[6]  } >
	<  F11[3]  : {  F8[11]  } >
	<  F11[4]  : {  F8[16]  } >
	<  F11[5]  : {  F8[5]  } >
	<  F11[6]  : {  F8[10]  } >
	<  F11[7]  : {  F8[15]  } >
	<  F11[8]  : {  F8[4]  } >
	<  F11[9]  : {  F8[9]  } >
	<  F11[10]  : {  F8[14]  } >
	<  F11[11]  : {  F8[3]  } >
	<  F11[12]  : {  F8[8]  } >
	<  F11[13]  : {  F8[13]  } >
	<  F11[14]  : {  F8[2]  } >
	<  F11[15]  : {  F8[7]  } >
	<  F11[16]  : {  F8[12]  } >
/>

< F12 > < linear > < FINDPARITY > <
	<  F12[1]  : {  ( F10[1] )  : F_PARITY ( F10[1] )  } >
	<  F12[2]  : {  ( F10[2] )  : F_PARITY ( F10[2] )  } >
	<  F12[3]  : {  ( F10[3] )  : F_PARITY ( F10[3] )  } >
	<  F12[4]  : {  ( F10[4] )  : F_PARITY ( F10[4] )  } >
	<  F12[5]  : {  ( F10[5] )  : F_PARITY ( F10[5] )  } >
	<  F12[6]  : {  ( F10[6] )  : F_PARITY ( F10[6] )  } >
	<  F12[7]  : {  ( F10[7] )  : F_PARITY ( F10[7] )  } >
	<  F12[8]  : {  ( F10[8] )  : F_PARITY ( F10[8] )  } >
	<  F12[9]  : {  ( F10[9] )  : F_PARITY ( F10[9] )  } >
	<  F12[10]  : {  ( F10[10] )  : F_PARITY ( F10[10] )  } >
	<  F12[11]  : {  ( F10[11] )  : F_PARITY ( F10[11] )  } >
	<  F12[12]  : {  ( F10[12] )  : F_PARITY ( F10[12] )  } >
	<  F12[13]  : {  ( F10[13] )  : F_PARITY ( F10[13] )  } >
	<  F12[14]  : {  ( F10[14] )  : F_PARITY ( F10[14] )  } >
	<  F12[15]  : {  ( F10[15] )  : F_PARITY ( F10[15] )  } >
	<  F12[16]  : {  ( F10[16] )  : F_PARITY ( F10[16] )  } >
/>

< F13 > <  linear > < CMP > <
	<  F13[1]  : {  ( F11[1] , F12[1] )  : F_ECMP ( F11[1] , F12[1] )  } >
	<  F13[2]  : {  ( F11[2] , F12[2] )  : F_ECMP ( F11[2] , F12[2] )  } >
	<  F13[3]  : {  ( F11[3] , F12[3] )  : F_ECMP ( F11[3] , F12[3] )  } >
	<  F13[4]  : {  ( F11[4] , F12[4] )  : F_ECMP ( F11[4] , F12[4] )  } >
	<  F13[5]  : {  ( F11[5] , F12[5] )  : F_ECMP ( F11[5] , F12[5] )  } >
	<  F13[6]  : {  ( F11[6] , F12[6] )  : F_ECMP ( F11[6] , F12[6] )  } >
	<  F13[7]  : {  ( F11[7] , F12[7] )  : F_ECMP ( F11[7] , F12[7] )  } >
	<  F13[8]  : {  ( F11[8] , F12[8] )  : F_ECMP ( F11[8] , F12[8] )  } >
	<  F13[9]  : {  ( F11[9] , F12[9] )  : F_ECMP ( F11[9] , F12[9] )  } >
	<  F13[10]  : {  ( F11[10] , F12[10] )  : F_ECMP ( F11[10] , F12[10] )  } >
	<  F13[11]  : {  ( F11[11] , F12[11] )  : F_ECMP ( F11[11] , F12[11] )  } >
	<  F13[12]  : {  ( F11[12] , F12[12] )  : F_ECMP ( F11[12] , F12[12] )  } >
	<  F13[13]  : {  ( F11[13] , F12[13] )  : F_ECMP ( F11[13] , F12[13] )  } >
	<  F13[14]  : {  ( F11[14] , F12[14] )  : F_ECMP ( F11[14] , F12[14] )  } >
	<  F13[15]  : {  ( F11[15] , F12[15] )  : F_ECMP ( F11[15] , F12[15] )  } >
	<  F13[16]  : {  ( F11[16] , F12[16] )  : F_ECMP ( F11[16] , F12[16] )  } >
/>

< F14 > < linear > < MDS > <
	<  F14[1]  : {  ( F10[1] ,F10[2] ,F10[3] ,F10[4]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[1] ) , F_MUL3 ( F10[2] ) ) , F_XOR ( F10[3] , F10[4] ) )  } >
	<  F14[2]  : {  ( F10[1] ,F10[2] ,F10[3] ,F10[4]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[2] ) , F_MUL3 ( F10[3] ) ) , F_XOR ( F10[1] , F10[4] ) )  } >
	<  F14[3]  : {  ( F10[1] ,F10[2] ,F10[3] ,F10[4]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[3] ) , F_MUL3 ( F10[4] ) ) , F_XOR ( F10[1] , F10[2] ) )  } >
	<  F14[4]  : {  ( F10[1] ,F10[2] ,F10[3] ,F10[4]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[4] ) , F_MUL3 ( F10[1] ) ) , F_XOR ( F10[2] , F10[3] ) )  } >
	<  F14[5]  : {  ( F10[5] ,F10[6] ,F10[7] ,F10[8]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[5] ) , F_MUL3 ( F10[6] ) ) , F_XOR ( F10[7] , F10[8] ) )  } >
	<  F14[6]  : {  ( F10[5] ,F10[6] ,F10[7] ,F10[8]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[6] ) , F_MUL3 ( F10[7] ) ) , F_XOR ( F10[5] , F10[8] ) )  } >
	<  F14[7]  : {  ( F10[5] ,F10[6] ,F10[7] ,F10[8]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[7] ) , F_MUL3 ( F10[8] ) ) , F_XOR ( F10[5] , F10[6] ) )  } >
	<  F14[8]  : {  ( F10[5] ,F10[6] ,F10[7] ,F10[8]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[8] ) , F_MUL3 ( F10[5] ) ) , F_XOR ( F10[6] , F10[7] ) )  } >
	<  F14[9]  : {  ( F10[9] ,F10[10] ,F10[11] ,F10[12]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[9] ) , F_MUL3 ( F10[10] ) ) , F_XOR ( F10[11] , F10[12] ) )  } >
	<  F14[10]  : {  ( F10[9] ,F10[10] ,F10[11] ,F10[12]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[10] ) , F_MUL3 ( F10[11] ) ) , F_XOR ( F10[9] , F10[12] ) )  } >
	<  F14[11]  : {  ( F10[9] ,F10[10] ,F10[11] ,F10[12]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[11] ) , F_MUL3 ( F10[12] ) ) , F_XOR ( F10[9] , F10[10] ) )  } >
	<  F14[12]  : {  ( F10[9] ,F10[10] ,F10[11] ,F10[12]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[12] ) , F_MUL3 ( F10[9] ) ) , F_XOR ( F10[10] , F10[11] ) )  } >
	<  F14[13]  : {  ( F10[13] ,F10[14] ,F10[15] ,F10[16]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[13] ) , F_MUL3 ( F10[14] ) ) , F_XOR ( F10[15] , F10[16] ) )  } >
	<  F14[14]  : {  ( F10[13] ,F10[14] ,F10[15] ,F10[16]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[14] ) , F_MUL3 ( F10[15] ) ) , F_XOR ( F10[13] , F10[16] ) )  } >
	<  F14[15]  : {  ( F10[13] ,F10[14] ,F10[15] ,F10[16]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[15] ) , F_MUL3 ( F10[16] ) ) , F_XOR ( F10[13] , F10[14] ) )  } >
	<  F14[16]  : {  ( F10[13] ,F10[14] ,F10[15] ,F10[16]  )  :  F_XOR ( F_XOR ( F_MUL2 ( F10[16] ) , F_MUL3 ( F10[13] ) ) , F_XOR ( F10[14] , F10[15] ) )  } >

/>

< F15 > < linear > < PREDICTPARITY > <
	<  F15[1]  : {  ( F10[1], F10[2], F10[3], F10[4],F12[1], F12[2], F12[3], F12[4] )  : F_GENPARITYMDS0( F10[1], F10[2], F10[3], F10[4],F12[1], F12[2], F12[3], F12[4] )  } >
	<  F15[2]  : {  ( F10[2], F10[3], F10[1], F10[4],F12[2], F12[3], F12[1], F12[4] )  : F_GENPARITYMDS0( F10[2], F10[3], F10[1], F10[4],F12[2], F12[3], F12[1], F12[4] )  } >
	<  F15[3]  : {  ( F10[3], F10[4], F10[1], F10[2],F12[3], F12[4], F12[1], F12[2] )  : F_GENPARITYMDS0( F10[3], F10[4], F10[1], F10[2],F12[3], F12[4], F12[1], F12[2] )  } >
	<  F15[4]  : {  ( F10[4], F10[1], F10[2], F10[3],F12[4], F12[1], F12[2], F12[3] )  : F_GENPARITYMDS0( F10[4], F10[1], F10[2], F10[3],F12[4], F12[1], F12[2], F12[3] )  } >
	<  F15[5]  : {  ( F10[5], F10[6], F10[7], F10[8],F12[5], F12[6], F12[7], F12[8] )  : F_GENPARITYMDS0( F10[5], F10[6], F10[7], F10[8],F12[5], F12[6], F12[7], F12[8] )  } >
	<  F15[6]  : {  ( F10[6], F10[7], F10[5], F10[8],F12[6], F12[7], F12[5], F12[8] )  : F_GENPARITYMDS0( F10[6], F10[7], F10[5], F10[8],F12[6], F12[7], F12[5], F12[8] )  } >
	<  F15[7]  : {  ( F10[7], F10[8], F10[5], F10[6],F12[7], F12[8], F12[5], F12[6] )  : F_GENPARITYMDS0( F10[7], F10[8], F10[5], F10[6],F12[7], F12[8], F12[5], F12[6] )  } >
	<  F15[8]  : {  ( F10[8], F10[5], F10[6], F10[7],F12[8], F12[5], F12[6], F12[7] )  : F_GENPARITYMDS0( F10[8], F10[5], F10[6], F10[7],F12[8], F12[5], F12[6], F12[7] )  } >
	<  F15[9]  : {  ( F10[9], F10[10], F10[11], F10[12],F12[9], F12[10], F12[11], F12[12] )  : F_GENPARITYMDS0( F10[9], F10[10], F10[11], F10[12],F12[9], F12[10], F12[11], F12[12] )  } >
	<  F15[10]  : {  ( F10[10], F10[11], F10[9], F10[12],F12[10], F12[11], F12[9], F12[12] )  : F_GENPARITYMDS0( F10[10], F10[11], F10[9], F10[12],F12[10], F12[11], F12[9], F12[12] )  } >
	<  F15[11]  : {  ( F10[11], F10[12], F10[9], F10[10],F12[11], F12[12], F12[9], F12[10] )  : F_GENPARITYMDS0( F10[11], F10[12], F10[9], F10[10],F12[11], F12[12], F12[9], F12[10] )  } >
	<  F15[12]  : {  ( F10[12], F10[9], F10[10], F10[11],F12[12], F12[9], F12[10], F12[11] )  : F_GENPARITYMDS0( F10[12], F10[9], F10[10], F10[11],F12[12], F12[9], F12[10], F12[11] )  } >
	<  F15[13]  : {  ( F10[13], F10[14], F10[15], F10[16],F12[13], F12[14], F12[15], F12[16] )  : F_GENPARITYMDS0( F10[13], F10[14], F10[15], F10[16],F12[13], F12[14], F12[15], F12[16] )  } >
	<  F15[14]  : {  ( F10[14], F10[15], F10[13], F10[16],F12[14], F12[15], F12[13], F12[16] )  : F_GENPARITYMDS0( F10[14], F10[15], F10[13], F10[16],F12[14], F12[15], F12[13], F12[16] )  } >
	<  F15[15]  : {  ( F10[15], F10[16], F10[13], F10[14],F12[15], F12[16], F12[13], F12[14] )  : F_GENPARITYMDS0( F10[15], F10[16], F10[13], F10[14],F12[15], F12[16], F12[13], F12[14] )  } >
	<  F15[16]  : {  ( F10[16], F10[13], F10[14], F10[15],F12[16], F12[13], F12[14], F12[15] )  : F_GENPARITYMDS0( F10[16], F10[13], F10[14], F10[15],F12[16], F12[13], F12[14], F12[15] )  } >
/>

< F16 > < linear > < FINDPARITY > <
	<  F16[1]  : {  ( F14[1] )  : F_PARITY ( F14[1] )  } >
	<  F16[2]  : {  ( F14[2] )  : F_PARITY ( F14[2] )  } >
	<  F16[3]  : {  ( F14[3] )  : F_PARITY ( F14[3] )  } >
	<  F16[4]  : {  ( F14[4] )  : F_PARITY ( F14[4] )  } >
	<  F16[5]  : {  ( F14[5] )  : F_PARITY ( F14[5] )  } >
	<  F16[6]  : {  ( F14[6] )  : F_PARITY ( F14[6] )  } >
	<  F16[7]  : {  ( F14[7] )  : F_PARITY ( F14[7] )  } >
	<  F16[8]  : {  ( F14[8] )  : F_PARITY ( F14[8] )  } >
	<  F16[9]  : {  ( F14[9] )  : F_PARITY ( F14[9] )  } >
	<  F16[10]  : {  ( F14[10] )  : F_PARITY ( F14[10] )  } >
	<  F16[11]  : {  ( F14[11] )  : F_PARITY ( F14[11] )  } >
	<  F16[12]  : {  ( F14[12] )  : F_PARITY ( F14[12] )  } >
	<  F16[13]  : {  ( F14[13] )  : F_PARITY ( F14[13] )  } >
	<  F16[14]  : {  ( F14[14] )  : F_PARITY ( F14[14] )  } >
	<  F16[15]  : {  ( F14[15] )  : F_PARITY ( F14[15] )  } >
	<  F16[16]  : {  ( F14[16] )  : F_PARITY ( F14[16] )  } >
/>

< F17 > <  linear > < CMP > <
	<  F17[1]  : {  ( F15[1] , F16[1] )  : F_ECMP ( F15[1] , F16[1] )  } >
	<  F17[2]  : {  ( F15[2] , F16[2] )  : F_ECMP ( F15[2] , F16[2] )  } >
	<  F17[3]  : {  ( F15[3] , F16[3] )  : F_ECMP ( F15[3] , F16[3] )  } >
	<  F17[4]  : {  ( F15[4] , F16[4] )  : F_ECMP ( F15[4] , F16[4] )  } >
	<  F17[5]  : {  ( F15[5] , F16[5] )  : F_ECMP ( F15[5] , F16[5] )  } >
	<  F17[6]  : {  ( F15[6] , F16[6] )  : F_ECMP ( F15[6] , F16[6] )  } >
	<  F17[7]  : {  ( F15[7] , F16[7] )  : F_ECMP ( F15[7] , F16[7] )  } >
	<  F17[8]  : {  ( F15[8] , F16[8] )  : F_ECMP ( F15[8] , F16[8] )  } >
	<  F17[9]  : {  ( F15[9] , F16[9] )  : F_ECMP ( F15[9] , F16[9] )  } >
	<  F17[10]  : {  ( F15[10] , F16[10] )  : F_ECMP ( F15[10] , F16[10] )  } >
	<  F17[11]  : {  ( F15[11] , F16[11] )  : F_ECMP ( F15[11] , F16[11] )  } >
	<  F17[12]  : {  ( F15[12] , F16[12] )  : F_ECMP ( F15[12] , F16[12] )  } >
	<  F17[13]  : {  ( F15[13] , F16[13] )  : F_ECMP ( F15[13] , F16[13] )  } >
	<  F17[14]  : {  ( F15[14] , F16[14] )  : F_ECMP ( F15[14] , F16[14] )  } >
	<  F17[15]  : {  ( F15[15] , F16[15] )  : F_ECMP ( F15[15] , F16[15] )  } >
	<  F17[16]  : {  ( F15[16] , F16[16] )  : F_ECMP ( F15[16] , F16[16] )  } >
/>

< end >
