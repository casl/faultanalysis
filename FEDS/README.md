> Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

> Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

> 1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

> 2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

> 3. Neither the name of IIT Madras  nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

### Folder Specifications

This folder contains source code and different block cipher implementations used to test FEDS framework.

__Examples__ : Folder include different types of Cipher implementations used to test FEDS.

Folder **example1** contains all the intermediate outputs

  1. Input **xxx.c** (c implementation of block cipher) and **bcs.c** (synthesised c code from BCS representation --Refer [SAFARI](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8634921), for details) 

  2. **FEMA.txt** contains the output of HLE Tool XFC (-- Refer [XFC](https://dl.acm.org/citation.cfm?id=3062340) for details)

  3. **info.txt** contains the information flow graph of BCS for one round of block cipher

  4. **E_Map.txt** contains the output of Equivalence Mapping for one round of the block cipher implementation

### Additional Inputs

  1. fun.txt contains the start and end of different functions

  2. iteration.txt contains the iterations lines from unrolled implementation of block cipher given in **IMP1-unrolled.c**

  These values are obtained from an LLVM parser, not part of FEDS.

__outs__ : Contains all the exploitable instructions for the given Block cipher implementation


### Description of files in Framework

 - FEDS.py 	---> Main file for FEDS framework
 - IFG.py 	---> Generate the Information Flow Graph
 - FaultMapping.py ---> Module for Equivalence Mapping in FEDS 
 - verify.py 	---> Module to insert constraints for Model Checker
 - FEME-Identification.py ---> Module for Identifying all FEME from FEMA
 - Traceback.py ---> Module to Identify merged and unmapped sub-operations
 - emap.py 	---> Generate E_Map.txt from the mapped output
 - fault_evaluation.py ---> Module for script to run the compiler phase of FEDS

### How to execute

 - execute "python FEDS.py Examples/examplexx IMPxx.c IMPxx-unrolled.c"

 - **User Input** : Details about Implementation

 	1. Lookup-table Implementations (Two-dimension)  : eg: example1, example2, example5
 	2. T-table Implementations 			 : eg: example3, example4
 	3. BitSliced Implementation			 : eg: example6
 	4. Lookup-table (One-Dimensionnsion)		 : eg: example7, example8

 - All intermediate outputs are stored in folder examplexx (Read the README file from the specific folder for other changes)

 - Exploitable instructions are stored in folder "IMPxx/outs/"

### Installations Steps for Model Checker
 
 - Model Checker used in FEDS is [CBMC](https://www.cprover.org/cbmc/) -- Please refer the installations steps from the website

### Installations Steps for LLVM Clang

 - Install LLVM Clang from [LLVM](https://llvm.org/docs/GettingStarted.html)
 - Bulid Type is Debug

### Versions of Tools used

 - CBMC --version 5.6 is used for Equivalence Mapping 

 - Python --version 2.7 is used for FEDS implementation

 - LLVM -- version 7.0.0svn

 - Clang -- version 7.0.0 is used for Fault Evaluation

**Note** 
example1, example2, example5 are executed with same input -- Refer IMP1 for more details
