###	Input-Output Description of FEDS 
###       All Rights Reserved        
---------------------------------------------------------------------------------

### Inputs to FEDS

1. bitsliced.c includes the SUT used in the analysis (refer Figure 3 of the manuscript)

2. aes.bcs contain the BCS representation of AES for one round including initial Key whitening (Refer Figure 3 of the manuscript)


**Values Need to be Defined**

 - Define Equivalence Mapping Values as 31 (line number in bcs.c), 17 (MAX_A), 263(line number in IMP1.c), 80(MAX_E) (These values are found empirically)

 - eg : **fm.equivalence_mapping(31,17,263,80,code,file1,type_imp)**


### Execute BitSliced Implementation

1. Compile c code : gcc bitsliced.c -o bs
2. Execute : ./bs -if in -of out -kf key
 	- Input is given in "in"
	- Output is written in "out"
	- Key in "key"


	
