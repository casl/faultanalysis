###	Input-Output Description of FEDS 
###       All Rights Reserved        
---------------------------------------------------------------------------------

### Inputs to FEDS

1. IMP3.c includes the SUT used in the analysis

2. bcs.c contain the synthesised code of BCS representation of AES


**Values Need to be Defined**

 - Define Equivalence Mapping Values as 31 (line number in bcs.c), 85 (MAX_A), 733(line number in IMP1.c), 10 (MAX_E) (These values are found empirically)

 - eg : **fm.equivalence_mapping(31,85,733,10,code,file1)**
	

