###	Input-Output Description of FEDS 
###       All Rights Reserved        
---------------------------------------------------------------------------------

### Inputs to FEDS

1. camellia.c includes the SUT used in the analysis

2. bcs.c contain the synthesised code of BCS representation of AES


**Values Need to be Defined**

 - Define Equivalence Mapping Values as 12 (line number in bcs.c), 10 (MAX_A), 32(line number in IMP1.c), 100 (MAX_E) (These values are found empirically)

 - eg : **fm.equivalence_mapping(12,10,32,100,code,file1,type_imp)**
	

