import sys, getopt
import array
import re
import os.path
from os import path

def arguments(file_name):
    if not path.exists(file_name):
	print "Fault Mapping output is not present\n"
        sys.exit(0)

#Generate E_Map from the output of fault Mapping
def generate_emap():

   	file_name = "final.txt"
    	output_file = "E_Map.txt"
    	arguments(file_name)

	G = [0] * 140
	prev_E = '{   ';
	f = open(output_file, "w")
	f.write("=====================       E_Map for Round 1          ======================\n\n");

	f.write("==============  Number of Model Checker Invocation : xxx ==============================\n\n");
	with open(file_name) as f1:
	    for i, line in enumerate(f1):
		#print line.strip()
		line_str = line.strip()
	    	#print line_str
	    	check = line_str.find('E')
	    	if check != -1:
	    		str1 = line_str[line_str.find('E'):line_str.find(':')]
			prev_E = prev_E + str1 + " : "
			#print str2, str1
			#exec("%s.append(%s)" % (str2,str1))

	    	else:
			check1 = line_str.find('G')
 			if check1 != -1:
			   str2 = line_str[line_str.find('G'):]
			   size = len(prev_E)
			   expression=prev_E[:size-2]
			   f.write( str2 + "\t-->\t" + expression + "  }\n")
			   #print str2, expression
			   # Assigning Array

			   #arraycontent=prev_E[4:size-2]
			   #print str2, arraycontent
			   # exec("%s.append(%s)" % (str2,))
			else:
			   prev_E = "{   "
			  # G_i = int(str1) - start_G
			  # E_i = int(prev_E) - start_E
			  # exec("G[%d] = %d" % (G_i,E_i))

	f1.close();


#generate_emap()
#print G32
