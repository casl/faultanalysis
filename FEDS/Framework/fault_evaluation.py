import sys, getopt
import array
import re
import os.path
from os import path

#Generate constraints for fault evaluation based on implementations

def fault_evaluation(path1,start,end):
	print "Generate the running script\n";
	f1 = open("evaluation.sh","w")
	string1 = """#!/bin/sh
TEST_CASES_DIR=/home/keerthi/Downloads/Software-Package/outs
BUILD_DIR=/media/keerthi/Work1/pa/llvm/build/
SHARED_LIB=$BUILD_DIR/lib/LLVMLiveness.so
 i=0
 j=0
"""
	f1.write(string1)
 	f1.write("PATH1="+path1+"\n")
 	f1.write("start="+start+"\n")
 	f1.write("end="+end+"\n")
	string2 = """
while [ $i -le  3 ]
do
  while [ $j -le 3 ]
 	do # add the lines from synthesised C
   	 echo "Fault Location.... state[$i][$j]"
   	 python generate.py $i $j $PATH1 $start $end 1
         echo "Finding the subgraph "
         clang -S -g -emit-llvm -O0 -Xclang -disable-O0-optnone fault-exploit.c -o o0.ll
         opt -S -loop-rotate -mem2reg -loop-unroll -unroll-count=100 -debug=0 o0.ll > s0.ll
         opt -S -load $SHARED_LIB -liveness s0.ll -o test.ll > $TEST_CASES_DIR/state$i$j.txt < input.txt
         j=$((j=j+1))
 	done
   j=0
   i=$((i=i+1))
done
echo "-----------Vulnerability detected successfully--------------------"
"""
	f1.write(string2)

def fault_evaluation_oned(path1,start,end):
	print "Generate the running script\n";
	f1 = open("evaluation.sh","w")
	string1 = """#!/bin/sh
TEST_CASES_DIR=/home/keerthi/Downloads/Software-Package/outs
BUILD_DIR=/media/keerthi/Work1/pa/llvm/build/
SHARED_LIB=$BUILD_DIR/lib/LLVMLiveness.so
 i=0
 j=0
"""
	f1.write(string1)
 	f1.write("PATH1="+path1+"\n")
 	f1.write("start="+start+"\n")
 	f1.write("end="+end+"\n")
	string2 = """
  while [ $j -le 15 ]
 	do # add the lines from synthesised C
   	 echo "Fault Location.... state[$j]"
   	 python generate.py $i $j $PATH1 $start $end 4
         echo "Finding the subgraph "
         clang -S -g -emit-llvm -O0 -Xclang -disable-O0-optnone fault-exploit.c -o o0.ll
         opt -S -loop-rotate -mem2reg -loop-unroll -unroll-count=100 -debug=0 o0.ll > s0.ll
         opt -S -load $SHARED_LIB -liveness s0.ll -o test.ll > $TEST_CASES_DIR/state$j.txt < input.txt
         j=$((j=j+1))
  done
echo "-----------Vulnerability detected successfully--------------------"
"""
	f1.write(string2)

def fault_evaluation_ttable(path1,start,end):
	print "Generate the running script\n";
	f1 = open("evaluation.sh","w")
	string1 = """#!/bin/sh
TEST_CASES_DIR=/home/keerthi/Downloads/Software-Package/outs
BUILD_DIR=/media/keerthi/Work1/pa/llvm/build/
SHARED_LIB=$BUILD_DIR/lib/LLVMLiveness.so
 i=0
 j=0
"""
	f1.write(string1)
 	f1.write("PATH1="+path1+"\n")
 	f1.write("start="+start+"\n")
 	f1.write("end="+end+"\n")
	string2 = """
while [ $i -le  3 ]
do
	# add the lines from synthesised C
	echo "Fault Location.... plaintext[$i]"
	python generate.py $i $j $PATH1 $start $end 2
	echo "Finding the subgraph "
	clang -S -g -emit-llvm -O0 -Xclang -disable-O0-optnone fault-exploit.c -o o0.ll
	opt -S -loop-rotate -mem2reg -loop-unroll -unroll-count=100 -debug=0 o0.ll > s0.ll
	opt -S -load $SHARED_LIB -liveness s0.ll -o test.ll > $TEST_CASES_DIR/plaintext$i.txt < input.txt
	i=$((i=i+1))
done
echo "-----------Vulnerability detected successfully--------------------"
"""
	f1.write(string2)




