import sys, getopt
import array
import re

#order of operations for AES (For Optimization)
# OP1 = ["AddroundKey","F1","F5","F9","F13","F17","F21","F25","F29","F33","F37","F40"]
# OP2 = ["SubByte","F2","F6","F10","F14","F18","F22","F26","F30","F34","F38"]
# OP3 = ["ShiftRows","F3","F7","F11","F15","F19","F23","F27","F31","F35","F39"]
# OP4 = ["MixColumns","F4","F8","F12","F16","F20","F24","F28","F32","F36"]

#order of operations for CLEFIA (For Optimization)
# OP1 = ["EndXOr", "F1","F92"]
# OP2 = ["swap","F6","F11","F16","F21","F26","F31","F36","F41","F46","F51","F56","F61","F66","F71","F76","F81","F86","F91" ]
# OP3 = ["clefiaKeyXor","F2","F7","F12","F17","F22","F27","F32","F37","F42","F47","F52","F57","F62","F67","F72","F77","F82","F87"]
# OP4 = ["subbyte", "F3","F8","F13","F18","F23","F28","F33","F38","F43","F48","F53","F58","F63","F68","F73","F78","F83","F88"]
# OP5 = ["mds","F4","F9","F14","F19","F24","F29","F34","F39","F44","F49","F54","F59","F64","F69","F74","F79","F84","F89"]
# OP6 = ["dxor","F5","F10","F15","F20","F25","F30","F35","F40","F45","F50","F55","F60","F65","F70","F75","F80","F85","F90"]

#order of operations for CAMELLIA (For Optimization)
OP1 = ["KeyXor","F2","F7","F12","F17","F22","F27","F44","F49","F54","F59","F64","F69","F86","F91","F96","F101","F106","F111" ]
OP2 = ["SubByte","F3","F8","F13","F18","F23","F28","F45","F50","F55","F60","F65","F70","F87","F92","F97","F102","F107","F112"]
OP3 = ["Diffusion","F4","F9","F14","F19","F24","F29","F46","F51","F56","F61","F66","F71","F88","F93","F98","F103","F108","F113"]
OP4 = ["dxor","F5","F10","F15","F20","F25","F30","F47","F52","F57","F62","F67","F72","F89","F94","F99","F104","F109","F114"]
OP5 = ["swap","F6","F11","F16","F21","F26","F31","F48","F53","F58","F63","F68","F73","F90","F95","F100","F105","F110","F115"] 
OP6 = ["flfuction","F32","F33","F34","F35","F36","F37","F38","F39","F40","F41","F42","F43","F74","F75","F76","F77","F78","F79","F80","F81","F82","F83","F84","F85"]

exploitable = []
exploitablelines = []
complexity = []
range_exploit = []
exploit_expression = []
exploitable_complexity = []

#Optimization in fault evalution phase
def exploitable_range(subop, comp):
	temp = []
	# Range is based on the number of sub-operations
	for i in range(1,7):
		exec 'opera = OP%s' %(i)
		if subop[0] in opera:
			temp.append(opera.index(subop[0]))
			temp.append(opera[0])
	temp.append(comp)

	#range_exploit.append(OP1.index(subop[0]), OP1[0], comp)
	range_exploit.append(temp)
	

#FEME identification
def feme_identification(path1, path2):

	with open(path2) as f1:
		for i, line in enumerate(f1):
			#print line_str
		    line_str = line.strip()
		    word = line_str.split(':')
		    exploitable.append(word[0])
		    complexity.append(word[1])
	f1.close()

	#print exploitable
	#print complexity
	checked = []
	with open(path1) as f1:
		for i, line in enumerate(f1):
			line_str = line.strip()
			#print i, line_str
			check = line_str.find('] =')
			check1 = line_str.find('F')
			if check != -1 and check1 != -1:
				word = line_str.split(']')
				subop = word[0].split('[')

				#print "Finding the exploitable sub-operations"
				expo = subop[0]+"["+str((int(subop[1])+1))+"] "
				if str(expo) in exploitable:
					gline = "G"+str(i+1)
					temp = str(expo)
					exp_i = exploitable.index(temp)
					comp = complexity[exp_i]
					exploitablelines.append(gline)
					exploitable_complexity.append(comp)

					if subop[0] not in checked: # Fixing the range
						checked.append(subop[0])
						exploitable_range(subop,comp)
				#else:
					#print  expo, ":", "Non Exploitable\n"
#optimizations
def optimize():
	complexity_values = []
	count = 1
	opt_range = []
	print "For Implementations Defined as Functions--Exploitable Function Range is :\n"

	print "********************Performizing some optimizations********************"
	for i in range(len(range_exploit)):
		temp = range_exploit[i]
		if temp[2] not in complexity_values:
			complexity_values.append(temp[2])
			print "Range",count,": (complexity =",temp[2],") ---------------------------"
			count += 1
			print temp[0], ":", temp[1]
		else:
			print temp[0],":", temp[1]
	print "---------------------------------------------------------"
	
	print "\nRange is distinquished based on complexity :"
	print complexity_values

	for temp in complexity_values:
		indices = [i for i, x in enumerate(range_exploit) if x[2] == temp]
		range1 = []
		for x in indices:
			temp1 = range_exploit[x]
			range1.append(temp1[0])
			range1.append(temp1[1])
		opt_range.append(range1)
	return opt_range

def optimize_expression(exploit_expression,exploitable_complexity):
	tcomp = []
	expression_range = []

	for x in exploitable_complexity:
		if x not in tcomp:
			tcomp.append(x)
	print tcomp
	for temp in tcomp:
		indices = [i for i, x in enumerate(exploitable_complexity) if x == temp]
		indices.sort()
		start = indices[0]
		end = indices[len(indices) - 1]
		range1 = (exploit_expression[start],exploit_expression[end])
		expression_range.append(range1)

	return expression_range

def find_expressions(path3):
	with open(path3) as f1:
		for i, line in enumerate(f1):
		    line_str = line.strip()
		    check = line_str.find('-->')
		    if check != -1:
		    	word = line_str.split('-->')
		    	#print word[0], ","
		    	temp = str(word[0]).strip()
		    	# print temp,","
		    	if str(temp) in exploitablelines:
		    		temp_exp = word[1].strip()
		    		#print temp_exp
		    		limit = temp_exp.split(':')
		    		for i in range(len(limit)):
		    			new = limit[i].replace("{","")
		    			new = new.replace("}","")
		    			new = new.strip()
		    			exploit_expression.append(new)



def feme_main(folder,type_imp):
	path1 = folder+"/bcs.c"
	path2 = folder+"/FEMA.txt"
	path3 = folder+"/final_E_Map.txt"
	feme_identification(path1,path2)
	#print exploitablelines
	opt_range= []
	exp_range = []
	print "\n\t<------------Exploitable Sub-operations------------>\n"
	print exploitablelines, "\n"
	print "\n\t<------------Finding the Exploitable Expression ( FEME )------------>\n"
	find_expressions(path3)
	opt_list = list(dict.fromkeys(exploit_expression))
	#print len(opt_list)
	print exploit_expression,"\n"

	if len(exploit_expression) != len(opt_list) and type_imp != "t-table":
		print "\n**Note** : Multiple occurance of same Expression is due to function call or merged sub-operations\n: So we need to consider the range from optimize function\n"
		print "Setting flag to 1"
		opt  = 1
		opt_range = optimize()
		print "Optimized Range : -->", opt_range
	else:
		print "Non overlapping sub-operations: So consider the exploitable_expressions\n"
		print "Setting flag to 0"
		exp_range = optimize_expression(exploit_expression,exploitable_complexity)
		print "Optimized Expression Range : -->", exp_range
		opt = 0

	return opt,opt_range, exp_range
