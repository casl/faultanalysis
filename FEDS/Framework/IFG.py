import sys, getopt
import array
import re


import os.path
from os import path

def arguments(file_name):
    if not path.exists(file_name):
	print "Folder is not present Or File Doesnot Exist\n"
	print "-----------INPUT FORMAT-------------------\n"
    	print "<PROGRAM> <folder> <start_line> <end_line>\n"
    	print "See README for details\n"
        sys.exit(0)

#Plot information flow graph for the bcs code
def information_graph(file_name,start_line,end_line):
	f = open("info.txt", "w")
	arguments(file_name)
	
	#print start_line
	#print end_line
	
	for d in xrange(0,end_line):    
		exec 'F%s = [0] * 16' %(d)

	with open(file_name) as f1:
	    for i, line in enumerate(f1):
		if i >= start_line and i < end_line:
		    line_str = line.strip()
		    #print line_str
		    check = line_str.find('] =')
		    if check != -1:
		    	str1 = line_str[0:line_str.find('[')]
			str2 = line_str[line_str.find('[')+1:line_str.find('] =')]
			exec("%s[%s] = %d" % (str1,str2,i+1))
  
	f1.close();
	val=0

	with open(file_name) as f1:
	    for i, line in enumerate(f1):
		if i >= start_line and i < end_line:
		    line_str = line.strip()
		    check = line_str.find(';')
		    if check != -1:
		       str1 = line_str[line_str.find('= ')+1:line_str.find(';')]
	 	       check1 = str1.find('F')
		       if check1 != -1:
			  new = str1[str1.find('F'):]
			  x = [m.start() for m in re.finditer('F', new)]
			  print >> f, i+1,"->",
			  for j in x: # Finding all the predecessor
	 		        new1 = new[j:] 
			  	str2 = new1[new1.find('F'):new1.find('[')]
			  	str3 = new1[new1.find('[')+1:new1.find(']')]
			  	#print str2,":",str3
			  	exec("print >> f,':', %s[%s]," % (str2,str3))
			  print >> f,":\n----------------------"
		 
	f1.close();

def Sort(tup): 
    return(sorted(tup, key = lambda x: int(x[1]), reverse = False)) 

def convert_tuple(folder):
	count = 0
	fun_list = []
	temp = []
	fun_file = folder + "/fun.txt"
	with open(fun_file) as f1:
		for i,line in enumerate(f1):
			line_str = line.strip()
			if count == 3:
			   fun_list.append(tuple(temp))
			   del temp[:]
			   count = 0
			temp.append(line_str)
			count = count + 1
	fun_list.append(temp)

	print(Sort(fun_list))

	return Sort(fun_list)

def determine_line(list1,folder):
	range_list = []
	for x in list1:
		temp = []
		for value in range(0,len(x),2):
			gcount1 = 0
			gcount2 = 0
			count1 = x[value]
			fun1 = x[value+1]
			iterate_file = folder + "/iteration.txt"
			with open(iterate_file) as f1:
				for i, line in enumerate(f1):
					line_str = line.strip()
					check = line_str.split(":")
					function = check[0].strip()
					fline = check[1].strip()
					if function == fun1:
						gcount1 +=1
						if gcount1 == count1:
							temp.append(fline)
		temp.sort()
		t_tuple = (temp[0], temp[len(temp)-1])
	    	range_list.append(t_tuple)	
	f1.close()
	print range_list
	return range_list

