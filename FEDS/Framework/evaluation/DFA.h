

#ifndef LLVM_TRANSFORMS_DFA_H
#define LLVM_TRANSFORMS_DFA_H

#include "llvm/InitializePasses.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/Support/raw_os_ostream.h"
#include <deque>
#include <stack>
#include <map>
#include <utility>
#include <vector>
typedef std::pair<std::vector<unsigned>, std::map<unsigned,std::set<std::list<unsigned>>>> ReturnInfo;

namespace llvm {


/*
 * This is the base class to represent information in a dataflow analysis.
 * For a specific analysis, you need to create a sublcass of it.
 */
class Info {
  public:
    Info() {}
    Info(const Info& other) {}
    virtual ~Info() {};

    /*
     * Print out the information
     *
     */
    virtual void print() = 0;
    static void printIndex();

    /*
     * Compare two pieces of information
     */
    static bool equals(Info * info1, Info * info2);

    static bool find1(unsigned index,Info *info2);

    static bool find2(std::vector<unsigned> index,Info *info2);
    /*
     * Join two pieces of information.  The third parameter points to the result.
     */
    static Info* join(Info * info1, Info * info2, Info * result);
    static Info* joinIndex(Info * info1, Info * info2, Info * result);

    static bool findmap(unsigned index, std::list<unsigned> list,Info *info2);
    static std::set<std::list<unsigned>> findmap1(unsigned index ,Info *info2);
};

/*
 * This is the base template class to represent the generic dataflow analysis framework
 * For a specific analysis, create subclass of this
 */
template <class Info, bool Direction>
class DataFlowAnalysis {

  private:

    typedef std::pair<unsigned, unsigned> Edge;
    // Index to instruction map
    std::map<unsigned, Instruction *> IndexToInstr;

    std::map<unsigned, GlobalVariable *> Global_IndexToInstr;
    // Instruction to index map
    std::map<Instruction *, unsigned> InstrToIndex;

    std::map<unsigned,Instruction*>LineToInstr;

    std::map<GlobalVariable *, unsigned> Global_InstrToIndex;
    // Edge to information map
    std::map<Edge, Info *> EdgeToInfo;
    // The bottom of the lattice
    Info Bottom;
    // The initial state of the analysis
    Info InitialState;
    // EntryInstr points to the first instruction to be processed in the analysis
    Instruction * EntryInstr;

    std::map<int,std::string> blockname;

    //storing map for all the function calls
    std::stack<std::map<unsigned, Instruction *>> IndexToInstr_stack;
    std::stack<std::map<Instruction *,unsigned>> InstrToIndex_stack;
    std::stack<std::map<unsigned, GlobalVariable *>> Global_IndexToInstr_stack;
    std::stack<std::map<GlobalVariable *,unsigned>> Global_InstrToIndex_stack;
    std::stack<std::map<Edge, Info *>>EdgeToInfo_stack;
    std::stack<std::map<int,std::string>>blockname_stack;

    /*
     * Assign an index to each instruction.
     * The results are stored in InstrToIndex and IndexToInstr.
     * A dummy node (nullptr) is added. It has index 0. This node has only one outgoing edge to EntryInstr.
     * The information of this edge is InitialState.
     * Any real instruction has an index > 0.
     *
     */
     void assignIndiceToInstrs(Module *Mod, Function * F) {

	InstrToIndex[nullptr] = 0;
	IndexToInstr[0] = nullptr;

	unsigned counter = 1;
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
		Instruction * instr = &*I;
		InstrToIndex[instr] = counter;
		IndexToInstr[counter] = instr;
		counter++;
	}
        for (auto I = Mod->global_begin(), E = Mod->global_end(); I!=E; ++I){
         	GlobalVariable *gv = &*I;
         	Global_InstrToIndex[gv] = counter;
         	Global_IndexToInstr[counter] = gv;
         	counter++;
        }
	return;
     }
     /*
      * Utility function:
      *   Get incoming edges of the instruction identified by index.
      *   IncomingEdges stores the indices of the source instructions of the incoming edges.
      */
	void getIncomingEdges(unsigned index, std::vector<unsigned> * IncomingEdges) {
		assert(IncomingEdges->size() == 0 && "IncomingEdges should be empty.");

		for (auto const &it : EdgeToInfo) {
			if (it.first.second == index)
				IncomingEdges->push_back(it.first.first);
			}
			return;
		}

     /*
      * Utility function:
      *   Get incoming edges of the instruction identified by index.
      *   OutgoingEdges stores the indices of the destination instructions of the outgoing edges.
      */
	void getOutgoingEdges(unsigned index, std::vector<unsigned> * OutgoingEdges) {
		assert(OutgoingEdges->size() == 0 && "OutgoingEdges should be empty.");

		for (auto const &it : EdgeToInfo) {
			if (it.first.first == index)
				OutgoingEdges->push_back(it.first.second);
			}
			return;
		}

     /*
      * Utility function:
      *   Insert an edge to EdgeToInfo.
      *   The default initial value for each edge is bottom.
      */
	void addEdge(Instruction * src, Instruction * dst, Info * content) {
		Edge edge = std::make_pair(InstrToIndex[src], InstrToIndex[dst]);
		if (EdgeToInfo.count(edge) == 0)
			EdgeToInfo[edge] = content;
		return;
	}

     /*
      * Initialize EdgeToInfo and EntryInstr for a forward analysis.
      */
	void initializeForwardMap(Module *Mod, Function * func) {
		assignIndiceToInstrs(Mod,func);

		for (Function::iterator bi = func->begin(), e = func->end(); bi != e; ++bi) {
			BasicBlock * block = &*bi;

			Instruction * firstInstr = &(block->front());
			// Initialize incoming edges to the basic block
			for (auto pi = pred_begin(block), pe = pred_end(block); pi != pe; ++pi) {
				BasicBlock * prev = *pi;
				Instruction * src = (Instruction *)prev->getTerminator();
				Instruction * dst = firstInstr;
				addEdge(src, dst, &Bottom);
			}

			// If there is at least one phi node, add an edge from the first phi node
			// to the first non-phi node instruction in the basic block.
			if (isa<PHINode>(firstInstr)) {
				addEdge(firstInstr, block->getFirstNonPHI(), &Bottom);
			}

			// Initialize edges within the basic block
			for (auto ii = block->begin(), ie = block->end(); ii != ie; ++ii) {
				Instruction * instr = &*ii;
				if (isa<PHINode>(instr))
					continue;
				if (instr == (Instruction *)block->getTerminator())
					break;
				Instruction * next = instr->getNextNode();
				addEdge(instr, next, &Bottom);
			}

			// Initialize outgoing edges of the basic block
			Instruction * term = (Instruction *)block->getTerminator();
			for (auto si = succ_begin(block), se = succ_end(block); si != se; ++si) {
				BasicBlock * succ = *si;
				Instruction * next = &(succ->front());
				addEdge(term, next, &Bottom);
			}

		}
		EntryInstr = (Instruction *) &((func->front()).front());
		addEdge(nullptr, EntryInstr, &InitialState);

		return;
	}

	/*
	 * Initialize EdgeToInfo and EntryInstr for a backward analysis.
	 * 
	 */
	void initializeBackwardMap(Module *Mod, Function * func) {
      		assignIndiceToInstrs(Mod,func);
      		EntryInstr = (Instruction *) (func -> back()).getTerminator();
      		addEdge(nullptr, EntryInstr, &InitialState);

      		for (auto rit = func -> end(); rit != func -> begin();) {
        		--rit;
        		BasicBlock *block = &*rit;
        		Instruction *lastInstr = block -> getTerminator();

        		for (auto si = succ_begin(block); si != succ_end(block); ++si) {
          			BasicBlock *succ = *si;
          			Instruction *src = &succ -> front();
          			addEdge(src, lastInstr, &Bottom);
        		}

        		Instruction *prevInstr = lastInstr;
        		auto ri = block -> rbegin();
        		++ri;
        		for (; ri != block -> rend(); ++ri) {
          			Instruction *instr = &*ri;
          			if (instr == &block -> front()) {
            				addEdge(prevInstr, instr, &Bottom);
            				break;
          			}
          			if (isa<PHINode>(instr)) {
            				continue;
          			}
          			addEdge(prevInstr, instr, &Bottom);
          			prevInstr = instr;
        		}

        		Instruction *firstInstr = &block -> front();
        		for (auto pi = pred_begin(block); pi != pred_end(block); ++pi) {
          			BasicBlock *pred = *pi;
          			Instruction *next = pred -> getTerminator();
          			addEdge(firstInstr, next, &Bottom);
        		}
      		}
      		return;
	}

    /*
     * The flow function.
     *   Instruction I: the IR instruction to be processed.
     *   std::vector<unsigned> & IncomingEdges: the vector of the indices of the source instructions of the incoming edges.
     *   std::vector<unsigned> & OutgoingEdges: the vector of indices of the source instructions of the outgoing edges.
     *   std::vector<Info *> & Infos: the vector of the newly computed information for each outgoing eages.
     *
     */
    virtual void flowfunction(Instruction * I, std::vector<unsigned> & IncomingEdges, std::vector<unsigned> & OutgoingEdges, std::vector<Info *> & Infos) = 0;

    public:
    	DataFlowAnalysis(Info & bottom, Info & initialState) :
    	Bottom(bottom), InitialState(initialState),EntryInstr(nullptr) {}

    	virtual ~DataFlowAnalysis() {}

    	/*
    	 * Print out the analysis results.
    	 *
    	 */
    	void print() {
		for (auto const &it : EdgeToInfo) {
			errs() << "Edge " << it.first.first << "->" "Edge " << it.first.second << ":";
			(it.second)->print();
		}
    	}

    	/*
    	 * This function implements the work list algorithm in the following steps:
    	 * (1) Initialize info of each edge to bottom
    	 * (2) Initialize the worklist
    	 * (3) Compute until the worklist is empty
   	 *
     	 */
    	void runWorklistAlgorithm(Module *Mod, Function * func) {
    		std::deque<unsigned> worklist;
      
      		unsigned count = 1;
        	for(Function::iterator bb = func->begin();bb!=func->end();bb++){
          		std::string name = bb->getName().str();
          		for(BasicBlock::iterator bi = bb->begin(); bi!= bb->end();bi++){
            			blockname[count] = name;
            			count++;
          		}
        	}

    		// (1) Initialize info of each edge to bottom
    		if (Direction){
    			initializeForwardMap(Mod,func); 
      		}
    		else{
    			initializeBackwardMap(Mod,func); 
      		}
    		assert(EntryInstr != nullptr && "Entry instruction is null.");

    		// (2) Initialize the work list
		for (int i = 1; i < (int) IndexToInstr.size()-1; i++) { 
        		worklist.push_back(i);
      		}

    		// (3) Compute until the work list is empty
      		while (!worklist.empty()) {
        		unsigned n = worklist.back();
        		worklist.pop_back();
        		std::vector<unsigned> incomingEdges;
        		getIncomingEdges(n, &incomingEdges);

        		std::vector<unsigned> outgoingEdges;
        		getOutgoingEdges(n, &outgoingEdges);

        		std::vector<Info *> infos;
        		flowfunction(IndexToInstr[n], incomingEdges, outgoingEdges, infos);
        		for (int i = 0; i < (int) outgoingEdges.size(); i++) {
          			Edge edge(n, outgoingEdges[i]);
          			if (!Info::equals(EdgeToInfo[edge], infos[i])) {
            				EdgeToInfo[edge] = infos[i];
            				worklist.push_back(outgoingEdges[i]);
          			}
        		}

      		}
   	// print();
    }

   ReturnInfo runWorklistAlgorithm2(Module *Mod, Function * func, std::vector<unsigned>index, std::map<unsigned,std::set<std::list<unsigned>>> ArgSet) {

      errs() <<"-------------------------"<< func->getName() << "--------------------------------------\n";
      IndexToInstr_stack.push(IndexToInstr);
      InstrToIndex_stack.push(InstrToIndex);
      Global_InstrToIndex_stack.push(Global_InstrToIndex);
      Global_IndexToInstr_stack.push(Global_IndexToInstr);
      EdgeToInfo_stack.push(EdgeToInfo);
      blockname_stack.push(blockname);
      InstrToIndex.clear();
      IndexToInstr.clear();
      Global_InstrToIndex.clear();
      Global_IndexToInstr.clear();
      EdgeToInfo.clear();
      blockname.clear();

      std::deque<unsigned> worklist1;
      std::vector<unsigned> fun_instr;
      std::map<unsigned,std::set<std::list<unsigned>>> fun_set;

      unsigned count = 1;
      for(Function::iterator bb = func->begin();bb!=func->end();bb++){
      	  std::string name = bb->getName().str();
          for(BasicBlock::iterator bi = bb->begin(); bi!= bb->end();bi++){
          	blockname[count] = name;
            	count++;
          }
      }

      // (1) Initialize info of each edge to bottom
      if (Direction){
        initializeForwardMap(Mod,func); 
      }
      else{
        initializeBackwardMap(Mod,func); 
      }
      assert(EntryInstr != nullptr && "Entry instruction is null.");

      // (2) Initialize the work list
      for (int i = 1; i < (int) IndexToInstr.size(); i++) { 
      	worklist1.push_back(i);
      }
      int last_index = (int) IndexToInstr.size()-1;
      Info *info = new Info(index,ArgSet);

      // (3) Compute until the work list is empty
      while (!worklist1.empty()) {
        unsigned n = worklist1.back();
        worklist1.pop_back();
        std::vector<unsigned> incomingEdges;
        getIncomingEdges(n, &incomingEdges);

        std::vector<unsigned> outgoingEdges;
        getOutgoingEdges(n, &outgoingEdges);

        std::vector<Info *> infos;
        flowfunction(IndexToInstr[n], incomingEdges, outgoingEdges, infos);

        for (int i = 0; i < (int) outgoingEdges.size(); i++) {
        	Edge edge(n, outgoingEdges[i]);
          	//For last instruction, relevant variable is the arguments.
          	if(n==last_index){
            		if(!Info::equals(EdgeToInfo[edge],info)){
              			EdgeToInfo[edge] = info;
              			worklist1.push_back(outgoingEdges[i]);
            		}
          	}
          	else{
            		if (!Info::equals(EdgeToInfo[edge], infos[i])) {
              			EdgeToInfo[edge] = infos[i];
              			worklist1.push_back(outgoingEdges[i]);
              			if(n==2){ // Returning from the function
                  			for(auto it = index.begin();it != index.end() ; it++){
                     				if(!Info::find1(*it,infos[i])){
                      					fun_instr.push_back(*it);
                       					std::set<std::list<unsigned>> ss = Info::findmap1(*it, infos[i]);
                       					if(ss.size() > 0){
                          					fun_set[*it] = ss;
                        				}
                    				}
                  			}
              			}
            		}
         	}
        }// End of Outgoing edges

      }
      EdgeToInfo = EdgeToInfo_stack.top();
      EdgeToInfo_stack.pop();
      IndexToInstr = IndexToInstr_stack.top();
      IndexToInstr_stack.pop();
      InstrToIndex = InstrToIndex_stack.top();
      InstrToIndex_stack.pop();
      Global_IndexToInstr = Global_IndexToInstr_stack.top();
      Global_IndexToInstr_stack.pop();
      Global_InstrToIndex = Global_InstrToIndex_stack.top();
      Global_InstrToIndex_stack.pop();
      blockname = blockname_stack.top();
      blockname_stack.pop();

      ReturnInfo rinfo = std::make_pair(fun_instr, fun_set);

     return rinfo;
    }

   ReturnInfo runWorklistAlgorithm3(Module *Mod, Function * func, std::vector<unsigned>index, std::map<unsigned,std::set<std::list<unsigned>>> ArgSet) {

      //Finding the Global Instruction for the function and also the indexes
      std::vector<GlobalVariable*>Global;
      std::map<GlobalVariable*,std::set<std::list<unsigned>>> Global_map;
      for(auto it = index.begin(); it != index.end();it++){
      	GlobalVariable *gv = Global_IndexToInstr[*it];
        Global.push_back(gv);
      }
      for(auto it = ArgSet.begin(); it != ArgSet.end();it++){
      	GlobalVariable *gv = Global_IndexToInstr[it->first];
        Global_map[gv] = it->second;
      }

      errs() <<"-------------------------"<< func->getName() << "--------------------------------------\n";
      IndexToInstr_stack.push(IndexToInstr);
      InstrToIndex_stack.push(InstrToIndex);
      Global_InstrToIndex_stack.push(Global_InstrToIndex);
      Global_IndexToInstr_stack.push(Global_IndexToInstr);
      EdgeToInfo_stack.push(EdgeToInfo);
      blockname_stack.push(blockname);
      InstrToIndex.clear();
      IndexToInstr.clear();
      Global_InstrToIndex.clear();
      Global_IndexToInstr.clear();
      EdgeToInfo.clear();
      blockname.clear();

      std::deque<unsigned> worklist1;
      //return pair contents
      std::vector<unsigned> fun_instr;
      std::map<unsigned,std::set<std::list<unsigned>>> fun_set;
      
      unsigned count = 1;
      for(Function::iterator bb = func->begin();bb!=func->end();bb++){
          std::string name = bb->getName().str();
          for(BasicBlock::iterator bi = bb->begin(); bi!= bb->end();bi++){
          	blockname[count] = name;
            	count++;
          }
      }

      // (1) Initialize info of each edge to bottom
      if (Direction){
        initializeForwardMap(Mod,func); 
      }
      else{
        initializeBackwardMap(Mod,func); 
      }

      //Clear the index variables
      //Add the new index for the new function
      index.clear();
      ArgSet.clear();

      for(auto it = Global.begin(); it!=Global.end(); it++){
        unsigned i = Global_InstrToIndex[*it];
        index.push_back(i);
      }
      for(auto it = Global_map.begin(); it!=Global_map.end(); it++){
        unsigned i = Global_InstrToIndex[it->first];
        ArgSet[i] = it->second;
      }

      assert(EntryInstr != nullptr && "Entry instruction is null.");

      // (2) Initialize the work list
      for (int i = 1; i < (int) IndexToInstr.size(); i++) {
        worklist1.push_back(i);
      }

      int last_index = (int) IndexToInstr.size()-1;
      Info *info = new Info(index,ArgSet);
       

      // (3) Compute until the work list is empty
      while (!worklist1.empty()) {
        unsigned n = worklist1.back();
        worklist1.pop_back();
        std::vector<unsigned> incomingEdges;
        getIncomingEdges(n, &incomingEdges);

        std::vector<unsigned> outgoingEdges;
        getOutgoingEdges(n, &outgoingEdges);

        std::vector<Info *> infos;
        flowfunction(IndexToInstr[n], incomingEdges, outgoingEdges, infos);

        for (int i = 0; i < (int) outgoingEdges.size(); i++) {
        	Edge edge(n, outgoingEdges[i]);
          	//For last instruction, relevant variable is the arguments.
        	if(n==last_index){
            		if(!Info::equals(EdgeToInfo[edge],info)){
              			EdgeToInfo[edge] = info;
              			worklist1.push_back(outgoingEdges[i]);
            		}
          	}
          	else{
            		if (!Info::equals(EdgeToInfo[edge], infos[i])) {
              			EdgeToInfo[edge] = infos[i];
              			worklist1.push_back(outgoingEdges[i]);
              			if(n==2){ // Returning from the function
                  			for(auto it = index.begin();it != index.end() ; it++){
                     				if(Info::find1(*it,infos[i])){
                      					fun_instr.push_back(*it);
                      					std::set<std::list<unsigned>> ss = Info::findmap1(*it, infos[i]);
                       					if(ss.size() > 0){
                          					fun_set[*it] = ss;
                        				}
                    				}
                  			}
              			}
            		}
         	}

        }// End of Outgoing edges

      }
      std::vector<GlobalVariable*>Global1;
      std::map<GlobalVariable*,std::set<std::list<unsigned>>> Global_map1;

      for(auto it = fun_instr.begin(); it != fun_instr.end();it++){
        GlobalVariable *gv = Global_IndexToInstr[*it];
        Global1.push_back(gv);
      }
      for(auto it = fun_set.begin(); it != fun_set.end();it++){
        GlobalVariable *gv = Global_IndexToInstr[it->first];
        Global_map1[gv] = it->second;
      }
     // print();
      EdgeToInfo = EdgeToInfo_stack.top();
      EdgeToInfo_stack.pop();
      IndexToInstr = IndexToInstr_stack.top();
      IndexToInstr_stack.pop();
      InstrToIndex = InstrToIndex_stack.top();
      InstrToIndex_stack.pop();
      Global_IndexToInstr = Global_IndexToInstr_stack.top();
      Global_IndexToInstr_stack.pop();
      Global_InstrToIndex = Global_InstrToIndex_stack.top();
      Global_InstrToIndex_stack.pop();
      blockname = blockname_stack.top();
      blockname_stack.pop();

      fun_instr.clear();
      fun_set.clear();
      for(auto it = Global1.begin(); it!=Global1.end(); it++){
        unsigned i = Global_InstrToIndex[*it];
        fun_instr.push_back(i);
      }

      for(auto it = Global_map1.begin(); it!=Global_map1.end(); it++){
        unsigned i = Global_InstrToIndex[it->first];
        fun_set[i] = it->second;
      }

      ReturnInfo rinfo = std::make_pair(fun_instr, fun_set);
     return rinfo;
    }


    std::map<Instruction *, unsigned> getInstrToIndex() {
      return InstrToIndex;
    }
    std::map<unsigned, Instruction *> getIndexToInstr() {
      return IndexToInstr;
    }
    std::map<Edge, Info *> getEdgeToInfo() {
      return EdgeToInfo;
    }
    std::map<int,std::string> getBlockname() {
      return blockname;
    }
    std::map<GlobalVariable *,unsigned> getGlobalInstrToIndex(){
      return Global_InstrToIndex;
    }
    std::map<unsigned,GlobalVariable *> getGlobalIndexToInstr(){
      return Global_IndexToInstr;
    }
};



}
#endif // End LLVM_DFA_H
